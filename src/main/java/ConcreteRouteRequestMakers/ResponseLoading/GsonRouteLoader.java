package ConcreteRouteRequestMakers.ResponseLoading;

import AbstractRouteMapping.TmbApiRouteLookupResponse;
import com.google.gson.reflect.TypeToken;
import utils.ClassLoading.GsonClassLoader;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class GsonRouteLoader extends GsonClassLoader<TmbApiRouteLookupResponse> {
    @Override
    public TmbApiRouteLookupResponse load(String json) {
        Type routeList = new TypeToken<ArrayList<TmbApiRouteLookupResponse>>(){}.getType();
        return gson.fromJson(json, routeList);
    }
}
