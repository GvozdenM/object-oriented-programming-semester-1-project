package ConcreteRouteRequestMakers.ResponseLoading;

import AbstractRouteMapping.TmbApiRouteLookupResponse;
import utils.ClassLoading.GsonClassLoader;
import utils.ClassLoading.GsonClassLoaderFactory;

import java.util.Collection;

public class GsonRouteLoaderFactory extends GsonClassLoaderFactory<TmbApiRouteLookupResponse> {
    @Override
    public GsonClassLoader<TmbApiRouteLookupResponse> createLoader() {
        return new GsonRouteLoader();
    }
}
