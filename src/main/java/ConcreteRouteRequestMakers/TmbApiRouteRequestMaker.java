package ConcreteRouteRequestMakers;

import AbstractNetworking.Connection.NetworkConnectionFactory;
import AbstractNetworking.RequestMaking.ApiRequestMaker;
import AbstractRouteMapping.TmbApiRouteLookupRequest;
import AbstractRouteMapping.TmbApiRouteLookupResponse;
import utils.ClassLoading.GsonClassLoaderFactory;
import utils.TmbUrlConstructionDirectors.Abstract.TmpApiUrlDirectorFactory;


public class TmbApiRouteRequestMaker extends ApiRequestMaker<TmbApiRouteLookupRequest, TmbApiRouteLookupResponse> {
    public TmbApiRouteRequestMaker(TmpApiUrlDirectorFactory<TmbApiRouteLookupRequest> urlDirectorFactory,
                                   NetworkConnectionFactory networkConnectionFactory,
                                   GsonClassLoaderFactory<TmbApiRouteLookupResponse> gsonClassLoaderFactory) {
        super(urlDirectorFactory, networkConnectionFactory, gsonClassLoaderFactory);
    }
}
