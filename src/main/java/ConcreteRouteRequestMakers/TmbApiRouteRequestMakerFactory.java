package ConcreteRouteRequestMakers;

import AbstractNetworking.RequestMaking.ApiRequestMaker;
import AbstractNetworking.RequestMaking.ApiRequestMakerFactory;
import AbstractRouteMapping.TmbApiRouteLookupRequest;
import AbstractRouteMapping.TmbApiRouteLookupResponse;
import ConcreteRouteRequestMakers.ResponseLoading.GsonRouteLoaderFactory;
import Main.Main;
import utils.TmbUrlConstructionDirectors.concrete.RouteLookupUrlDirectorFactory;

import java.util.Collection;

public class TmbApiRouteRequestMakerFactory extends ApiRequestMakerFactory<TmbApiRouteLookupRequest, TmbApiRouteLookupResponse> {

    @Override
    public ApiRequestMaker<TmbApiRouteLookupRequest, TmbApiRouteLookupResponse> createRequestMaker() {
        return new TmbApiRouteRequestMaker(
                new RouteLookupUrlDirectorFactory(),
                Main.networkConnectionFactory,
                new GsonRouteLoaderFactory()
        );
    }
}
