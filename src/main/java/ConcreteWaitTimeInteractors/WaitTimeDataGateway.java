package ConcreteWaitTimeInteractors;

import AbstractWaitTimeModels.WaitTime;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

public interface WaitTimeDataGateway {
    List<WaitTime> getWaitTime(String stopCode) throws IOException, URISyntaxException;
}
