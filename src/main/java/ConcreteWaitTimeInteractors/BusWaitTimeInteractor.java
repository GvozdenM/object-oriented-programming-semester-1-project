package ConcreteWaitTimeInteractors;

import AbstractWaitTimeInteractor.LineWaitTimeResponse;
import AbstractWaitTimeInteractor.WaitTimeRequester;
import AbstractWaitTimeInteractor.WaitTimeResponse;
import AbstractWaitTimeMapping.WaitTimeDataGatewayFactory;
import AbstractWaitTimeModels.WaitTime;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;

public class BusWaitTimeInteractor implements WaitTimeRequester {
    private WaitTimeDataGateway dataGateway;

    public BusWaitTimeInteractor(WaitTimeDataGatewayFactory dataGatewayFactory) {
        this.dataGateway = dataGatewayFactory.createGateway();
    }

    @Override
    public WaitTimeResponse getWaitTime(String stopCode) throws IOException, URISyntaxException {
        List<WaitTime> waitTimes = this.dataGateway.getWaitTime(stopCode);
        WaitTimeResponse response;

        if(0 != waitTimes.size()) {
            response = new WaitTimeResponse(true);
            response.setError("No Error");

            for(WaitTime waitTime : waitTimes) {
                LineWaitTimeResponse lineWaitTime = new LineWaitTimeResponse(
                        waitTime.getLineName(),
                        waitTime.getWaitTimeMinutes()
                );

                response.addArrivingLine(lineWaitTime);
            }

        }
        else {
            response = new WaitTimeResponse(false);
            response.setError("No Buses are arriving at this stop!");
            response.setLinesArriving(new LinkedList<>());
            response.setFavoriteStop(false);
        }

        return response;
    }
}
