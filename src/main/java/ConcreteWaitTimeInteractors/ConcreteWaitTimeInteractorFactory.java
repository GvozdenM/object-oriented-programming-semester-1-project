package ConcreteWaitTimeInteractors;

import AbstractWaitTimeInteractor.WaitTimeInteractorFactory;
import ConcreteWaitTimeInteractors.BusWaitTimeInteractor;
import AbstractWaitTimeInteractor.WaitTimeRequester;
import Main.Main;

public class ConcreteWaitTimeInteractorFactory extends WaitTimeInteractorFactory {
    @Override
    public WaitTimeRequester createWaitTimeInteractor() {
        return new BusWaitTimeInteractor(Main.waitTimeDataGatewayFactory);
    }
}
