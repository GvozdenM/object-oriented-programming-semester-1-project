package ConcreteRouteMapping;

import AbstractLocationModels.Coords;
import AbstractLocationModels.PrimitiveLocation;
import AbstractNetworking.RequestMaking.ApiRequestMaker;
import AbstractRouteMapping.ApiRouteModels.RouteItinerary;
import AbstractRouteMapping.ApiRouteModels.TmbRouteLeg;
import AbstractRouteMapping.TmbApiRouteLookupRequest;
import AbstractRouteMapping.TmbApiRouteLookupResponse;
import ConcreteRouteLookupInteractors.RouteDataGateway;
import ConcreteRouteModels.SimpleRoute;
import ConcreteRouteModels.SimpleRouteLeg;
import ConcreteRouteRequestMakers.TmbApiRouteRequestMakerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.*;


public class RouteLookupTmbApiMapper implements RouteDataGateway<LinkedHashMap<String, String>, SimpleRouteLeg, SimpleRoute> {
    private TmbApiRouteRequestMakerFactory tmbApiRouteRequestMakerFactory;

    public RouteLookupTmbApiMapper(TmbApiRouteRequestMakerFactory tmbApiRouteRequestMakerFactory) {
        this.tmbApiRouteRequestMakerFactory = tmbApiRouteRequestMakerFactory;
    }

    @Override
    public Collection<SimpleRoute> getRoutesByDepartureTime(Coords origin,
                                                            Coords destination,
                                                            int maxWalkingDistance,
                                                            Date time) throws IOException, URISyntaxException {
        TmbApiRouteLookupRequest lookupRequest = this.configureRequestModel(
                origin,
                destination,
                maxWalkingDistance,
                time
        );

        ApiRequestMaker<TmbApiRouteLookupRequest, TmbApiRouteLookupResponse> requestMaker =
                this.tmbApiRouteRequestMakerFactory.createRequestMaker();

        TmbApiRouteLookupResponse lookupResponse = requestMaker.process(lookupRequest);
        return this.transformResponseModel(lookupResponse);
    }

    @Override
    public Collection<SimpleRoute> getRoutesByArrivalTime(Coords origin,
                                                          Coords destination,
                                                          int maxWalkingDistance,
                                                          Date time) {
        return null;
    }

    private TmbApiRouteLookupRequest configureRequestModel(Coords origin,
                                                           Coords destination,
                                                           int maxWalkingDistance,
                                                           Date time) {
        String originLocation = this.transformCoordinatesToString(origin);
        String destinationLocation = this.transformCoordinatesToString(destination);

        return new TmbApiRouteLookupRequest(
                originLocation,
                destinationLocation,
                time.toString(),
                maxWalkingDistance
        );
    }

    private String transformCoordinatesToString(Coords coords) {
        return coords.getLatitude() + "," + coords.getLongitude();
    }


    private Collection<SimpleRoute> transformResponseModel(TmbApiRouteLookupResponse response) {
        List<SimpleRoute> routes = new ArrayList<>();

        for(RouteItinerary itinerary : response.getPlan().getItineraries()) {
            int legCount = itinerary.getLegs().size();
            PrimitiveLocation origin = transformToPrimitiveLocation(itinerary.getLegs().get(0));
            PrimitiveLocation destination = transformToPrimitiveLocation(itinerary.getLegs().get(legCount));

            SimpleRoute route = new SimpleRoute();
            route.setOrigin(origin);
            route.setDestination(destination);
            route.setLegs(this.transformToSimpleLegs(itinerary.getLegs()));
            route.setDuration(itinerary.getDuration());
            route.setWalkingDistance((int) itinerary.getWalkDistance());

            routes.add(route);
        }

        return routes;
    }

    private List<SimpleRouteLeg> transformToSimpleLegs(Collection<TmbRouteLeg> apiLegs) {
        List<SimpleRouteLeg> legs = new ArrayList<>();

        for(TmbRouteLeg leg : apiLegs) {
            Map<String, String> metadata = new LinkedHashMap<>();
            metadata.put("routeName", leg.getRouteShortName());
            metadata.put("routeLongName", leg.getRouteLongName());

            if(leg.isTransitLeg()) {
                metadata.put("stopCode", leg.getFrom().getStopCode());
            }

            legs.add(
                    new SimpleRouteLeg(
                        leg.getDuration(),
                        leg.getDistance(),
                        leg.isTransitLeg(),
                        metadata
                    )
            );
        }

        return legs;
    }

    private PrimitiveLocation transformToPrimitiveLocation(TmbRouteLeg leg) {
        PrimitiveLocation location = new PrimitiveLocation();
        location.setName(leg.getFrom().getName());
        location.setLatitude(leg.getFrom().getLatitude());
        location.setLongitude(leg.getFrom().getLongitude());

        return location;
    }
}
