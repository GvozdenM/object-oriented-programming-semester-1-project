package ConcreteLocationMapping;

import AbstractLocationMapping.DuplicateLocationException;
import LocationStoragePool.loading.builders.AbstractLocationBuilder;
import LocationStoragePool.loading.builders.LocationBuilder;
import ConcreteLocationLookupInteractors.LocationDataGateway;
import AbstractLocationModels.DescribedLocation;
import Main.Main;

import java.io.IOException;
import java.util.*;

public class LocationDataBankMapper implements LocationDataGateway<DescribedLocation> {
    public void addLocation(DescribedLocation location) throws DuplicateLocationException, IOException {
        ArrayList<Double> coords = new ArrayList<>();
        coords.add(location.getLatitude());
        coords.add(location.getLongitude());
        AbstractLocationBuilder builder = new LocationBuilder(
                location.getName(),
                location.getDescription(),
                coords,
                Boolean.parseBoolean(location.getMetadata().get("isUserDefined"))
        );

        Main.locationDataGatewaySingleton.addLocation(builder.build());
    }

    private DescribedLocation convertLocation(Map<String, String > basicLocation) {
        DescribedLocation describedLocation = new DescribedLocation();
        Map<String, String> metadata = new LinkedHashMap<>(basicLocation);

        describedLocation.setName(metadata.get("name"));
        metadata.remove("name");

        describedLocation.setLongitude(Double.parseDouble(metadata.get("longitude")));
        metadata.remove("longitude");

        describedLocation.setLatitude(Double.parseDouble(metadata.get("latitude")));
        metadata.remove("latitude");

        describedLocation.setDescription(metadata.get("description"));
        metadata.remove("description");

        describedLocation.setMetadata(metadata);

        return describedLocation;
    }

    private List<DescribedLocation> convertLocations(List<Map<String, String>> basicLocations) {
        List<DescribedLocation> describedLocations = new LinkedList<>();

        for(Map<String, String> basicLocation : basicLocations) {
            describedLocations.add(this.convertLocation(basicLocation));
        }

        return describedLocations;
    }

    public List<DescribedLocation> getAllLocations() throws IOException {
        return convertLocations(
                Main.locationDataGatewaySingleton.getAllLocations()
        );
    }

    public List<DescribedLocation> getUserDefinedLocations() throws IOException {
        return convertLocations(
                Main.locationDataGatewaySingleton.getUserLocations()
        );
    }

    @Override
    public DescribedLocation getLocation(String name) throws IOException {
        return this.convertLocation(
                Main.locationDataGatewaySingleton.getLocation(name)
        );
    }
}
