package ConcreteLocationMapping;

import AbstractLocationMapping.UnifiedLocationDataGatewayFactory;
import AbstractLocationModels.DescribedLocation;
import ConcreteLocationLookupInteractors.LocationDataGateway;

public class ConcreteLocationMapperFactory extends UnifiedLocationDataGatewayFactory<DescribedLocation> {
    @Override
    public LocationDataGateway<DescribedLocation> createUnifiedLocationDataGateway() {
        return new LocationDataBankMapper();
    }
}
