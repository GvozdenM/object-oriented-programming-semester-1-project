package ConcreteRouteLookupInteractors;

import AbstractLocationModels.Coords;
import AbstractRouteModels.Route;
import AbstractRouteModels.RouteLeg;


import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.Date;

public interface RouteDataGateway<K, L extends RouteLeg<K>, T extends Route<L>> {
    Collection<T> getRoutesByDepartureTime(Coords origin, Coords destination, int maxWalkingDistance, Date time) throws IOException, URISyntaxException;
    Collection<T> getRoutesByArrivalTime(Coords origin, Coords destination, int maxWalkingDistance, Date time);
}
