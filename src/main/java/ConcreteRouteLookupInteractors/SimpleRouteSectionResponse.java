package ConcreteRouteLookupInteractors;

import AbstractRouteLookupInteractors.RouteSectionResponse;

import java.util.LinkedHashMap;
import java.util.Map;

public class SimpleRouteSectionResponse extends RouteSectionResponse {
    public SimpleRouteSectionResponse(int sectionTotalTimeMinutes,
                                      boolean usesTransit,
                                      Map<String, String> metadata) {
        super(
                sectionTotalTimeMinutes,
                usesTransit,
                new LinkedHashMap<>(metadata)
        );
    }
}
