package ConcreteRouteLookupInteractors;

import AbstractRouteLookupInteractors.RouteLookupResponse;

import java.util.Collection;
import java.util.LinkedList;

public class SimpleRouteLookupResponse extends RouteLookupResponse {
    public SimpleRouteLookupResponse(boolean success,
                                     String error,
                                     String originName,
                                     String destinationName,
                                     int totalTimeMinutes,
                                     Collection<SimpleRouteSectionResponse> sectionList) {
        super(
                success,
                error,
                originName,
                destinationName,
                totalTimeMinutes,
                new LinkedList<>(sectionList)
        );
    }
}
