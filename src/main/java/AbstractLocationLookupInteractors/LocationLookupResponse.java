package AbstractLocationLookupInteractors;

import java.util.Collection;

public abstract class LocationLookupResponse {
    private boolean anyExist;
    private int numFound;
    private Collection<LookedUpLocation> foundLocations;


    public LocationLookupResponse(Collection<LookedUpLocation> foundLocations) {
        this.foundLocations = foundLocations;
    }

    public boolean anyExist() {
        return anyExist;
    }

    public int getNumFound() {
        return numFound;
    }

    public Collection<LookedUpLocation> getFoundLocations() {
        return foundLocations;
    }

    public void addLocation(LookedUpLocation l) {
        this.foundLocations.add(l);
    }

    public boolean isAnyExist() {
        return anyExist;
    }

    public void setAnyExist(boolean anyExist) {
        this.anyExist = anyExist;
    }

    public void setNumFound(int numFound) {
        this.numFound = numFound;
    }

    public void setFoundLocations(Collection<LookedUpLocation> foundLocations) {
        this.foundLocations = foundLocations;
    }
}
