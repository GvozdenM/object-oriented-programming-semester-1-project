package AbstractLocationLookupInteractors;

public abstract class LocationLookupRequesterFactory<T> {
    public abstract LocationLookupRequester<T> createInteractor();
}
