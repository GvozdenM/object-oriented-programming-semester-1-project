package AbstractLocationLookupInteractors;

import java.util.Map;

public abstract class LookedUpLocation {
    private String locationName;
    private String locationDescription;
    private double latitude;
    private double longitude;
    private Map<String, String> metadata;

    public LookedUpLocation(String locationName,
                            String locationDescription,
                            Map<String, String> metadata,
                            double latitude,
                            double longitude) {
        this.locationName = locationName;
        this.locationDescription = locationDescription;
        this.metadata = metadata;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getLocationName() {
        return locationName;
    }

    public String getLocationDescription() {
        return locationDescription;
    }

    public Map<String, String> getMetadata() {
        return metadata;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }
}
