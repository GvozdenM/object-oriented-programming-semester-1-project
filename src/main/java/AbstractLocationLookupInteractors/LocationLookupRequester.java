package AbstractLocationLookupInteractors;

import java.io.IOException;

public interface LocationLookupRequester<T> {
    T getLocation(String locationName) throws IOException;
}
