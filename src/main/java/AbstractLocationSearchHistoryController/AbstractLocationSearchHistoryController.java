package AbstractLocationSearchHistoryController;

import AbstractUserStorage.UserLocationSearchGateway;

public interface AbstractLocationSearchHistoryController {
    void process(UserLocationSearchGateway searchGateway);
}
