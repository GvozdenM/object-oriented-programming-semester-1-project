package AbstractMenuPresenter;

import java.util.Collection;

public interface AbstractMenuView {
    void displayMenu(Collection<String> menuElements);
}
