package AbstractMenuPresenter;


public abstract class AbstractMenuViewFactory {
    public abstract AbstractMenuView create();
}
