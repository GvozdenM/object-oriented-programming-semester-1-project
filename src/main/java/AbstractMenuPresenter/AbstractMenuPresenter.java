package AbstractMenuPresenter;


public abstract class AbstractMenuPresenter {
    protected AbstractMenuViewFactory viewFactory;

    public AbstractMenuPresenter(AbstractMenuViewFactory factory) {
        this.viewFactory = factory;
    }

    public abstract void presentMenu();
}
