package ConcreteLocationSearchHistoryView;

import AbstractLocationSearchHistoryPresenter.AbstractLocationSerachHistoryView;

import java.util.Collection;

public class LocationSearchHistoryView implements AbstractLocationSerachHistoryView {

    public void displayHistory(Collection<String> searchedLocations) {
        StringBuilder output = new StringBuilder();
        output.append("Searched Locations: ");
        for(String locationName : searchedLocations) {
            output
                    .append("\n")
                    .append("\t- ")
                    .append(locationName);
        }
        System.out.println(output.toString());
    }

    public void displayNoHistory() {
        System.out.println("You have not searched for any locations!" +
                "\nTo search for one, access option 2 of the Main menu.");
    }
}
