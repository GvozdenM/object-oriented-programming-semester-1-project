package ConcreteLocationSearchHistoryView;

import AbstractLocationSearchHistoryPresenter.AbstractLocationSearchHistoryViewFactory;
import AbstractLocationSearchHistoryPresenter.AbstractLocationSerachHistoryView;

public class LocationSearchHistoryViewFactory extends AbstractLocationSearchHistoryViewFactory {
    @Override
    public AbstractLocationSerachHistoryView createView() {
        return new LocationSearchHistoryView();
    }
}
