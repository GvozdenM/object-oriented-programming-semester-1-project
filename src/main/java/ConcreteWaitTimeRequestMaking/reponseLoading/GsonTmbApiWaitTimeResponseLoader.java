package ConcreteWaitTimeRequestMaking.reponseLoading;


import AbstractWaitTimeMapping.TmbApiWaitTimeResponse;
import utils.ClassLoading.GsonClassLoader;

public class GsonTmbApiWaitTimeResponseLoader extends GsonClassLoader<TmbApiWaitTimeResponse> {
    @Override
    public TmbApiWaitTimeResponse load(String json) {
        return gson.fromJson(json, TmbApiWaitTimeResponse.class);
    }
}
