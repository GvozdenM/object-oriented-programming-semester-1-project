package ConcreteWaitTimeRequestMaking.reponseLoading;

import AbstractWaitTimeMapping.TmbApiWaitTimeResponse;
import utils.ClassLoading.GsonClassLoader;
import utils.ClassLoading.GsonClassLoaderFactory;

public class GsonTmbApiWaitTimeResponseLoaderFactory extends GsonClassLoaderFactory<TmbApiWaitTimeResponse> {
    @Override
    public GsonClassLoader<TmbApiWaitTimeResponse> createLoader() {
        return new GsonTmbApiWaitTimeResponseLoader();
    }
}
