package ConcreteWaitTimeRequestMaking;

import AbstractWaitTimeMapping.TmbApiWaitTimeRequester;
import AbstractWaitTimeMapping.TmbApiWaitTimeResponse;
import AbstractNetworking.Connection.NetworkConnectionFactory;

import AbstractNetworking.RequestMaking.ApiRequestMaker;
import utils.ClassLoading.GsonClassLoaderFactory;
import utils.TmbUrlConstructionDirectors.Abstract.TmpApiUrlDirectorFactory;


public class TmbApiWaitTimeRequestMaker extends ApiRequestMaker<String, TmbApiWaitTimeResponse> implements TmbApiWaitTimeRequester {

    public TmbApiWaitTimeRequestMaker(TmpApiUrlDirectorFactory<String> urlDirectorFactory,
                                      NetworkConnectionFactory networkConnectionFactory,
                                      GsonClassLoaderFactory<TmbApiWaitTimeResponse> gsonClassLoaderFactory) {
        super(urlDirectorFactory, networkConnectionFactory, gsonClassLoaderFactory);
    }
}
