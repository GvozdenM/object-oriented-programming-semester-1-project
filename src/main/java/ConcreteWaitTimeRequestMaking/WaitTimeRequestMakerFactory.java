package ConcreteWaitTimeRequestMaking;

import AbstractWaitTimeMapping.TmbApiWaitTimeResponse;
import Main.Main;
import AbstractNetworking.RequestMaking.ApiRequestMaker;
import AbstractNetworking.RequestMaking.ApiRequestMakerFactory;

public class WaitTimeRequestMakerFactory extends ApiRequestMakerFactory<String, TmbApiWaitTimeResponse> {

    @Override
    public ApiRequestMaker<String, TmbApiWaitTimeResponse> createRequestMaker() {
        return new TmbApiWaitTimeRequestMaker(
                Main.waitTimeUrlDirectorFactory,
                Main.networkConnectionFactory,
                Main.tmbApiWaitTimeResponseGsonClassLoaderFactory
        );
    }
}
