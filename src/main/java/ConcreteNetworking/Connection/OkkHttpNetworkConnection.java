package ConcreteNetworking.Connection;

import AbstractNetworking.Connection.NetworkConnection;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;
import java.net.URL;
import java.util.Objects;

public class OkkHttpNetworkConnection implements NetworkConnection {
    @Override
    public String get(URL request) throws IOException {

        OkHttpClient client = new OkHttpClient();
        Request _request = new Request.Builder()
                .url(request)
                .build();
        Response _responses = client.newCall(_request).execute();

        if(_responses.body() != null) {
            return Objects.requireNonNull(_responses.body()).string();
        }
        else {
            return "";
        }

    }
}
