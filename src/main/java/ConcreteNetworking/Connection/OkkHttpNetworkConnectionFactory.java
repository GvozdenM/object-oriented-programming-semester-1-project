package ConcreteNetworking.Connection;

import AbstractNetworking.Connection.NetworkConnection;
import AbstractNetworking.Connection.NetworkConnectionFactory;

public class OkkHttpNetworkConnectionFactory extends NetworkConnectionFactory {
    @Override
    public NetworkConnection createNetworkConnection() {
        return new OkkHttpNetworkConnection();
    }
}
