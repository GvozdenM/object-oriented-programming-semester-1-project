package ConcreteMenuView;

import AbstractMenuPresenter.AbstractMenuViewFactory;

public class ConcreteMenuViewFactory extends AbstractMenuViewFactory {
    @Override
    public ConcreteMenuView create() {
        return new ConcreteMenuView();
    }
}
