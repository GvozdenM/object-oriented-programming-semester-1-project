package ConcreteMenuView;

import AbstractMenuPresenter.AbstractMenuView;

import java.util.Collection;

public class ConcreteMenuView implements AbstractMenuView {
    @Override
    public void displayMenu(Collection<String> menuElements) {
        for(String menuElement : menuElements) {
            System.out.println(menuElement);
        }
        System.out.println("\nSelected option: ");
    }
}
