package ConcreteWaitTimePresenters;

import AbstractWaitTimePresenters.BusWaitTimePresenter;
import AbstractWaitTimePresenters.WaitTimePresenterFactory;
import Main.Main;

public class ScreenWaitTimePresenterFactory extends WaitTimePresenterFactory {
    @Override
    public BusWaitTimePresenter createWaitTimePresenter() {
        return new BusWaitTimeScreenPresenter(Main.waitTimeViewFactory);
    }
}
