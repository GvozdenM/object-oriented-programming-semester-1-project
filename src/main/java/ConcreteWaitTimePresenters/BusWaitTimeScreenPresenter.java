package ConcreteWaitTimePresenters;

import AbstractWaitTimePresenters.BusWaitTimePresenter;
import AbstractWaitTimePresenters.BusWaitTimeViewModel;
import ConcreteWaitTimeViews.viewInterfaces.BusWaitTimeView;
import ConcreteWaitTimeViews.viewInterfaces.BusWaitTimeViewFactory;
import AbstractWaitTimeInteractor.LineWaitTimeResponse;
import AbstractWaitTimeInteractor.WaitTimeResponse;

import java.util.LinkedHashMap;
import java.util.Map;

public class BusWaitTimeScreenPresenter implements BusWaitTimePresenter {
    private BusWaitTimeView view;

    public BusWaitTimeScreenPresenter(BusWaitTimeViewFactory viewFactory) {
        this.view = viewFactory.createBusWaitTimeView();
    }

    protected BusWaitTimeViewModel initViewModel(WaitTimeResponse waitTimeResponse) {
        BusWaitTimeViewModel viewModel = new BusWaitTimeViewModel(waitTimeResponse.isFavoriteStop());
        Map<String, String> waitingTimes = new LinkedHashMap<>();

        for(LineWaitTimeResponse lineArriving : waitTimeResponse.getLinesArriving()) {
            waitingTimes.put(
                    lineArriving.getLineName(),
                    Integer.toString(lineArriving.getWaitTimeMinutes())
            );
        }

        viewModel.setWaitTimes(waitingTimes);
        return viewModel;
    }

    @Override
    public void presentWaitTimes(WaitTimeResponse waitTimeResponse) {
        BusWaitTimeViewModel viewModel = initViewModel(waitTimeResponse);
        this.view.displayBusWaitTimes(viewModel);
    }

    @Override
    public void presentInteractorError(WaitTimeResponse waitTimeResponse) {
        BusWaitTimeViewModel viewModel = initViewModel(waitTimeResponse);
        this.view.displayErrorMessage("ERROR: Unknown error occurred! Could not retrieve wait times!");
    }
}
