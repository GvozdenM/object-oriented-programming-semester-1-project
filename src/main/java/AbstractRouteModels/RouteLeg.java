package AbstractRouteModels;

import java.util.List;

public abstract class RouteLeg<T> {
    private int duration;
    private double distance;
    private boolean isTransitLeg;
    protected T metadata;

    public RouteLeg(int duration,
                    double distance,
                    boolean isTransitLeg,
                    T metadata) {
        this.duration = duration;
        this.distance = distance;
        this.isTransitLeg = isTransitLeg;
        this.metadata = metadata;
    }

    public abstract List<String> getMetadataList();

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public boolean isTransitLeg() {
        return isTransitLeg;
    }

    public void setTransitLeg(boolean transitLeg) {
        isTransitLeg = transitLeg;
    }

    public T getMetadata() {
        return metadata;
    }

    public void setMetadata(T metadata) {
        this.metadata = metadata;
    }
}
