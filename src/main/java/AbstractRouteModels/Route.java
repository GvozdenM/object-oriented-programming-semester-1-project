package AbstractRouteModels;

import AbstractLocationModels.PrimitiveLocation;

import java.util.List;

public abstract class Route<Leg> {
    private PrimitiveLocation origin;
    private PrimitiveLocation destination;
    private int duration;
    private int walkingDistance;
    private List<Leg> legs;

    public Route() {
    }

    public Route(PrimitiveLocation origin,
                 PrimitiveLocation destination,
                 int duration,
                 int walkingDistance,
                 List<Leg> legs) {
        this.origin = origin;
        this.destination = destination;
        this.duration = duration;
        this.walkingDistance = walkingDistance;
        this.legs = legs;
    }

    public PrimitiveLocation getOrigin() {
        return origin;
    }

    public void setOrigin(PrimitiveLocation origin) {
        this.origin = origin;
    }

    public PrimitiveLocation getDestination() {
        return destination;
    }

    public void setDestination(PrimitiveLocation destination) {
        this.destination = destination;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getWalkingDistance() {
        return walkingDistance;
    }

    public void setWalkingDistance(int walkingDistance) {
        this.walkingDistance = walkingDistance;
    }

    public List<Leg> getLegs() {
        return legs;
    }

    public void setLegs(List<Leg> legs) {
        this.legs = legs;
    }
}
