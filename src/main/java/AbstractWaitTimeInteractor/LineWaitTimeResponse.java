package AbstractWaitTimeInteractor;

public class LineWaitTimeResponse {
    private String lineName;
    private int waitTimeMinutes;

    public LineWaitTimeResponse(String lineName, int waitTimeMinutes) {
        this.lineName = lineName;
        this.waitTimeMinutes = waitTimeMinutes;
    }

    public String getLineName() {
        return lineName;
    }

    public void setLineName(String lineName) {
        this.lineName = lineName;
    }

    public int getWaitTimeMinutes() {
        return waitTimeMinutes;
    }

    public void setWaitTimeMinutes(int waitTimeMinutes) {
        this.waitTimeMinutes = waitTimeMinutes;
    }
}
