package AbstractWaitTimeInteractor;

import java.util.ArrayList;
import java.util.List;

public class WaitTimeResponse {
    private boolean success;
    private String error;
    private boolean isFavoriteStop;
    private List<LineWaitTimeResponse> linesArriving;

    public WaitTimeResponse(boolean success) {
        this.success = success;
        this.linesArriving = new ArrayList<>();
    }

    public WaitTimeResponse(boolean success,
                            String error,
                            boolean isUserFavoriteStop,
                            List<LineWaitTimeResponse> linesArriving) {
        this.success = success;
        this.error = error;
        this.isFavoriteStop = isUserFavoriteStop;
        this.linesArriving = new ArrayList<>(linesArriving);
    }

    public WaitTimeResponse(List<LineWaitTimeResponse> linesArriving) {
        this.linesArriving = new ArrayList<>(linesArriving);
    }

    public void setFavoriteStop(boolean favoriteStop) {
        isFavoriteStop = favoriteStop;
    }

    public boolean isFavoriteStop() {
        return isFavoriteStop;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public List<LineWaitTimeResponse> getLinesArriving() {
        return linesArriving;
    }

    public void setLinesArriving(List<LineWaitTimeResponse> linesArriving) {
        this.linesArriving = linesArriving;
    }

    public void addArrivingLine(LineWaitTimeResponse line) {
        this.linesArriving.add(line);
    }
}
