package AbstractWaitTimeInteractor;

import java.io.IOException;
import java.net.URISyntaxException;

public interface WaitTimeRequester {
    WaitTimeResponse getWaitTime(String stopCode) throws IOException, URISyntaxException;
}
