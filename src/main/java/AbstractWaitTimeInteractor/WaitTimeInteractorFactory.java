package AbstractWaitTimeInteractor;

import AbstractWaitTimeInteractor.WaitTimeRequester;

public abstract class WaitTimeInteractorFactory {
    public abstract WaitTimeRequester createWaitTimeInteractor();
}
