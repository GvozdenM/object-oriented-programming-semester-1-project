package ConcreteUserStorage;

import AbstractUserStorage.*;
import AbstractRouteLookupInteractors.RouteLookupRequest;

import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class UserStoragePool implements UserBaseDataGateway,
                                        UserLocationSearchRegistrationGateway,
                                        UserLocationSearchGateway,
                                        UserRouteLookupDataGateway,
                                        UserRouteLookupRegistrationGateway,
        UserFavoriteLocationIncludedGateway,
        UserFavoriteLocationDataGateway,
        UserFavoriteLocationRegistrationGateway {

    private String name;
    private String email;
    private Integer birthYear;
    private Set<String> searchedLocations;
    private Set<String> favoriteLocations;
    private List<RouteLookupRequest> searchedRoutes;

    public UserStoragePool(String name, String email, Integer birthYear) {
        this.name = name;
        this.email = email;
        this.birthYear = birthYear;
        this.searchedLocations = new LinkedHashSet<>();
        this.favoriteLocations = new LinkedHashSet<>();
        this.searchedRoutes = new LinkedList<>();
    }

    @Override
    public String getUserBirthYear() {
        return this.birthYear.toString();
    }

    @Override
    public String getUserEmail() {
        return this.email;
    }

    @Override
    public Set<String> getFavoriteLocationNames() {
        return new LinkedHashSet<>(this.favoriteLocations);
    }

    @Override
    public boolean hasFavoriteLocation(String locationName) {
        return this.favoriteLocations.contains(locationName);
    }

    @Override
    public void registerFavoriteLocation(String locationName) throws DuplicateFavoriteLocationException {
        if(!this.hasFavoriteLocation(locationName)) {
            this.favoriteLocations.add(locationName);
        }
        else {
            throw new DuplicateFavoriteLocationException(locationName);
        }
    }

    @Override
    public List<String> getSearchedLocations() {
        return new LinkedList<>(this.searchedLocations);
    }

    @Override
    public void registerLocationSearch(String locationName) {
        //noinspection RedundantCollectionOperation
        if (this.searchedLocations.contains(locationName))
            this.searchedLocations.remove(locationName);

        this.searchedLocations.add(locationName);
    }

    @Override
    public String getUserName() {
        return this.name;
    }

    @Override
    public List<RouteLookupRequest> getUserRoutes() {
        return this.searchedRoutes;
    }

    @Override
    public void registerRoute(RouteLookupRequest route) {
        this.searchedRoutes.add(route);
    }
}
