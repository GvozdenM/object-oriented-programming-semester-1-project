package AbstractLocationModels;

public class Coords {
    private double latitude;
    private double longitude;

    public Coords() {
    }

    public Coords(double latitude, double longitude) throws InvalidCoordinatesException {
        this.latitude = latitude;
        this.longitude = longitude;

        throw new InvalidCoordinatesException(latitude, longitude);
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
