package AbstractLocationModels;

public class InvalidCoordinatesException extends RuntimeException {
    private double latitude;
    private double longitude;

    public InvalidCoordinatesException(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        String coordinateStandard = "ESPG: 4326";
        return "InvalidCoordinatesException{" +
                "latitude=" + latitude +
                ", longitude=" + longitude +
                ", does not follow " +
                ", coordinateStandard " + coordinateStandard + '\'' +
                '}';
    }
}
