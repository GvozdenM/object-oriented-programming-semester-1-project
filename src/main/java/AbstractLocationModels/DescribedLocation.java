package AbstractLocationModels;

import java.util.HashMap;
import java.util.Map;

public class DescribedLocation extends PrimitiveLocation {
    private String description;
    private Map<String, String> metadata;

    public DescribedLocation() throws InvalidCoordinatesException {
        super();
    }

    public DescribedLocation(double latitude,
                             double longitude,
                             String name,
                             String description,
                             Map<String, String> metadata) throws InvalidCoordinatesException {

        super(latitude, longitude, name);
        this.description = description;
        this.metadata = metadata;
    }

    public DescribedLocation(double latitude,
                             double longitude,
                             String name,
                             String description) throws InvalidCoordinatesException {
        super(latitude, longitude, name);
        this.description = description;
        this.metadata = new HashMap<String, String>();
    }

    public Map<String, String> getMetadata() {
        return metadata;
    }

    public  void addMetadata(String dataName, String dataValue) {
        this.metadata.put(dataName, dataValue);
    }

    public String getLongDescription() {
        StringBuilder builder = new StringBuilder()
                .append(description);

        for(String prop : metadata.values()) {
            builder.append(prop);
        }

        return builder.toString();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setMetadata(Map<String, String> metadata) {
        this.metadata = metadata;
    }
}
