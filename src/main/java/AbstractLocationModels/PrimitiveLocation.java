package AbstractLocationModels;

public class PrimitiveLocation extends Coords {
    private String name;

    public PrimitiveLocation() {
    }

    public PrimitiveLocation(double latitude, double longitude, String name) throws InvalidCoordinatesException {
        super(latitude, longitude);
        this.name = new String(name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
