package ConcreteWaitTimeViews;

import ConcreteWaitTimeViews.viewInterfaces.BusWaitTimeViewFactory;
import ConcreteWaitTimeViews.viewInterfaces.BusWaitTimeView;

public class WaitTimeConsoleViewFactory extends BusWaitTimeViewFactory {
    @Override
    public BusWaitTimeView createBusWaitTimeView() {
        return new WaitTimeConsoleView();
    }
}
