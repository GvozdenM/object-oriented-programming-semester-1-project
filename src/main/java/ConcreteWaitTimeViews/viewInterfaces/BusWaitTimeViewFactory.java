package ConcreteWaitTimeViews.viewInterfaces;

public abstract class BusWaitTimeViewFactory {
    public abstract BusWaitTimeView createBusWaitTimeView();
}
