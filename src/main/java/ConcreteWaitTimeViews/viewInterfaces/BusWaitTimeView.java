package ConcreteWaitTimeViews.viewInterfaces;

import AbstractWaitTimePresenters.BusWaitTimeViewModel;

public interface BusWaitTimeView {
    void displayBusWaitTimes(BusWaitTimeViewModel busWaitTimes);

    void displayErrorMessage(String errorMessage);
}
