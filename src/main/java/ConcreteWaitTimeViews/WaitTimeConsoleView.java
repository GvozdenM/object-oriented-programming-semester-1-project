package ConcreteWaitTimeViews;

import ConcreteWaitTimeViews.viewInterfaces.BusWaitTimeView;
import AbstractWaitTimePresenters.BusWaitTimeViewModel;

import java.util.Map;

public class WaitTimeConsoleView implements BusWaitTimeView {
    public void displayBusWaitTimes(BusWaitTimeViewModel busWaitTimes) {
        StringBuilder builder = new StringBuilder();

        if(busWaitTimes.isFavoriteStop()) {
            builder.append("Favorite Stop!\n");
        }

        for(Map.Entry<String, String> entry : busWaitTimes.getWaitTimes().entrySet()) {
            builder
                    .append(entry.getKey())
                    .append(" - ")
                    .append(entry.getValue())
                    .append(" min")
                    .append("\n");
        }

        System.out.println(builder.toString());
    }

    public void displayErrorMessage(String errorMessage) {
        System.out.println(errorMessage);
    }
}
