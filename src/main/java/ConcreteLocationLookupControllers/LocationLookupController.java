package ConcreteLocationLookupControllers;

import AbstractLocationLookupInteractors.LocationLookupResponse;
import AbstractLocationLookupPresenters.LookedUpLocationPresenter;
import AbstractLocationLookupPresenters.LookedUpLocationPresenterFactory;
import AbstractLocationLookupInteractors.LocationLookupRequester;
import AbstractLocationLookupInteractors.LocationLookupRequesterFactory;
import AbstractLocationLookupControllers.SimpleLocationLookupController;
import AbstractUserStorage.UserLocationSearchGateway;
import AbstractUserStorage.UserLocationSearchRegistrationGateway;

import java.io.IOException;


public class LocationLookupController implements SimpleLocationLookupController {
    private LocationLookupRequesterFactory<LocationLookupResponse> interactorFactory;
    private LookedUpLocationPresenterFactory presenterFactory;
    private UserLocationSearchRegistrationGateway userLocationSearchRegistrationGateway;

    public LocationLookupController(LocationLookupRequesterFactory<LocationLookupResponse> interactorFactory,
                                    LookedUpLocationPresenterFactory presenterFactory,
                                    UserLocationSearchRegistrationGateway userLocationSearchGateway) {
        this.interactorFactory = interactorFactory;
        this.presenterFactory = presenterFactory;
        this.userLocationSearchRegistrationGateway = userLocationSearchGateway;
    }

    @Override
    public boolean process(String locationName) throws IOException {
        LocationLookupRequester<LocationLookupResponse> interactor = this.interactorFactory.createInteractor();
        LookedUpLocationPresenter presenter = this.presenterFactory.createPresenter();

        LocationLookupResponse response = interactor.getLocation(locationName);

        if(response.isAnyExist()) {
            presenter.presentFoundLocations(response.getFoundLocations());
            this.userLocationSearchRegistrationGateway.registerLocationSearch(locationName);
            return true;
        }
        else {
            presenter.presentNoLocationsFound();
            return false;
        }
    }
}
