package ConcreteLocationLookupControllers;

import AbstractUserStorage.UserLocationSearchGateway;
import AbstractUserStorage.UserLocationSearchRegistrationGateway;
import ConcreteLocationLookupInteractors.SimpleLocationLookupInteractorFactory;
import ConcreteLocationLookupPresenters.LocationLookupConsolePresenterFactory;
import Main.AbstractControllerFactory;
import Main.ControllerI;

public class LocationLookupControllerFactory  {
    public ControllerI<String> createController(UserLocationSearchRegistrationGateway userLocationSearchGateway) {
        return new LocationLookupController(
                new SimpleLocationLookupInteractorFactory(),
                new LocationLookupConsolePresenterFactory(),
                userLocationSearchGateway
        );
    }
}
