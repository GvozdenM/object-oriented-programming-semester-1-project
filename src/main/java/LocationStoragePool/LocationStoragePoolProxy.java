package LocationStoragePool;

import LocationStoragePool.loading.GsonLocationClassLoader;
import LocationStoragePool.loading.LoadableLocation;
import LocationStoragePool.loading.LocationFileLoader;
import LocationStoragePool.loading.transformation.LocationTransformationHandlerFactory;
import LocationStoragePool.loading.transformation.models.LocationTransformationHandler;
import AbstractLocationMapping.UnifiedLocationDataBank;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class LocationStoragePoolProxy implements UnifiedLocationDataBank {

    private DataModel locationStoragePool;
    private LocationFileLoader fileLoader;
    private LocationTransformationHandlerFactory locationTransformationHandlerFactory;

    public LocationStoragePoolProxy(LocationTransformationHandlerFactory locationTransformationHandlerFactory) {
        this.locationStoragePool = null;
        this.fileLoader = new LocationFileLoader();
        this.locationTransformationHandlerFactory = locationTransformationHandlerFactory;
    }

    private void spawnStoragePoolIfNeeded() throws IOException {
        if(!storagePoolIsSpawned()) {
            spawnStoragePool();
        }
    }

    private boolean storagePoolIsSpawned() {
        return null != locationStoragePool;
    }

    private void spawnStoragePool() throws IOException {
        File file = this.fileLoader.getDataFromResourceFile();
        GsonLocationClassLoader gsonLoader = new GsonLocationClassLoader();

        ArrayList<LoadableLocation> loadedLocations = gsonLoader.load(
                new String(
                        Files.readAllBytes(
                                Paths.get(
                                        file.getPath()
                                )
                        )
                )
        );

        this.locationStoragePool = sortLocations(loadedLocations);
    }

    private DataModel sortLocations(List<LoadableLocation> input) {
        DataModel bufferStoragePool = new DataModel();
        LocationTransformationHandler handlerChain =
                this.locationTransformationHandlerFactory.createLocationTransformationHandler();

        for(LoadableLocation location : input) {
            handlerChain.handleRequest(location, bufferStoragePool);
        }
        return bufferStoragePool;
    }

    public List<Map<String, String>> getAllLocations() throws IOException {
        spawnStoragePoolIfNeeded();
        return this.locationStoragePool.getAllLocations();
    }

    public List<Map<String, String>> getUserLocations() throws IOException {
        spawnStoragePoolIfNeeded();
        return this.locationStoragePool.getUserLocations();
    }

    public void addLocation(Map<String, String> location) throws IOException {
        spawnStoragePoolIfNeeded();
        this.locationStoragePool.addLocation(location);
    }

    @Override
    public Map<String, String> getLocation(String name) throws IOException {
        spawnStoragePoolIfNeeded();
        return this.locationStoragePool.getLocation(name);
    }

    @Override
    public void close() {
        if(storagePoolIsSpawned()) {
            this.locationStoragePool.close();
        }
    }

    public void addLocations(List<Map<String, String>> locations) throws IOException {
        spawnStoragePoolIfNeeded();
        locationStoragePool.addAllLocations(locations);
    }
}
