package LocationStoragePool;

import AbstractLocationMapping.UnifiedLocationDataBank;

import java.util.*;

public class DataModel implements UnifiedLocationDataBank {

    //  Name /  metadata name / metadata value
    private Map<String, Map<String, String>> locations;
    private List<Map<String, String>> userDefinedLocations;

    public DataModel() {
        this.locations = new LinkedHashMap<>();
        this.userDefinedLocations = new LinkedList<>();
    }

    public void addAllLocations(List<Map<String, String>> locations) {
        for(Map<String, String> location : locations) {
            this.addLocation(location);
        };
    }

    public List<Map<String, String>> getAllLocations() {
        return new ArrayList<>(this.locations.values());
    }

    public Map<String, String> getLocation(String name) {
        return this.locations.getOrDefault(name, null);
    }


    public Boolean locationExists(String name) {
        return this.locations.containsKey(name);
    }

    public List<Map<String, String>> getUserLocations() {
        return new LinkedList<>(userDefinedLocations);
    }

    public void addLocation(Map<String, String> location) {
        String name = location.get("name");

        if(!this.locationExists(name)) {
            this.locations.put(name, location);

            if("true".equals(location.get("isUserDefined"))) {
                this.userDefinedLocations.add(location);
            }
        }
    }

    @Override
    public void close() {
        //TODO
    }

}
