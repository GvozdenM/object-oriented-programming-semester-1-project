package LocationStoragePool.loading;

import java.io.File;
import java.io.IOException;
import java.net.URL;

public class LocationFileLoader {
    private final String defaultLocationFilePath = "localitzacions.json";
    private String filePath;

    public LocationFileLoader(String filepath) {
        this.filePath = new String(filepath);
    }

    public LocationFileLoader() {}

    public File getDataFromResourceFile() throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();

        String path =
                null != this.filePath ?
                      this.filePath :
                      defaultLocationFilePath;

        URL src = classLoader.getResource(path);

        if(src == null) {
            throw new IOException(path + " not found.");
        }
        else {
            return new File(src.getFile());
        }
    }
}
