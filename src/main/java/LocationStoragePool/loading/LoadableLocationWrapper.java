package LocationStoragePool.loading;

import java.util.ArrayList;
import java.util.List;

public class LoadableLocationWrapper {
    List<LoadableLocation> locations;

    public ArrayList<LoadableLocation> getLocations() {
        return new ArrayList<>(locations);
    }
}
