package LocationStoragePool.loading.builders;

import java.util.*;

public abstract class AbstractLocationBuilder {
    protected Map<String, String> location;


    public AbstractLocationBuilder(String locationName,
                           String locationDescription,
                           ArrayList<Double> coords,
                           boolean isUserDefined) {
        this.location = new LinkedHashMap<>();
        this.addName(locationName);
        this.addDescription(locationDescription);
        this.addCoords(coords);
    }


    public abstract void addName(String name);
    public abstract void addDescription(String description);
    public abstract void addCoords(ArrayList<Double> coords);
    public abstract void addStars(int stars);
    public abstract void addArchitect(String name);
    public abstract void addTags(List<String> characteristics);
    public abstract void addIsUserDefined(boolean isUserDefined);
    public abstract Map<String, String> build();
}
