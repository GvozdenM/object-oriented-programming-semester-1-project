package LocationStoragePool.loading.builders;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class LocationBuilder extends AbstractLocationBuilder {
    public LocationBuilder(String locationName,
                           String locationDescription,
                           ArrayList<Double> coords,
                           boolean isUserDefined) {
        super(locationName, locationDescription, coords, isUserDefined);
    }

    public LocationBuilder(String locationName,
                           String locationDescription,
                           ArrayList<Double> coords, boolean isUserDefined,
                           String architectName) {
        super(locationName, locationDescription, coords, isUserDefined);

        this.addArchitect(architectName);
    }

    public LocationBuilder(String locationName,
                           String locationDescription,
                           ArrayList<Double> coords, boolean isUserDefined,
                           int stars) {
        super(locationName, locationDescription, coords, isUserDefined);

        this.addStars(stars);
    }

    public LocationBuilder(String locationName,
                           String locationDescription,
                           ArrayList<Double> coords, boolean isUserDefined,
                           List<String> tags) {
        super(locationName, locationDescription, coords, isUserDefined);

        this.addTags(tags);
    }

    @Override
    public void addName(String name) {
        this.location.put("name", name);
    }

    @Override
    public void addDescription(String description) {
        this.location.put("description", description);
    }

    @Override
    public void addCoords(ArrayList<Double> coords) {
        this.location.put("latitude", coords.get(0).toString());
        this.location.put("longitude", coords.get(1).toString());
    }

    @Override
    public void addStars(int stars) {
        this.location.put("stars", Integer.toString(stars));
    }

    @Override
    public void addArchitect(String name) {
        this.location.put("architect", name);
    }

    @Override
    public void addTags(List<String> characteristics) {
        StringBuilder stringBuilder = new StringBuilder();

        for(String characteristic : characteristics) {
            stringBuilder
                    .append(characteristic)
                    .append(",");
        }

        this.location.put("tags", stringBuilder.toString());
    }

    @Override
    public void addIsUserDefined(boolean isUserDefined) {
        this.location.put("isUserDefined", Boolean.toString(isUserDefined));
    }

    @Override
    public Map<String, String> build() {
        return new LinkedHashMap<String, String>(this.location);
    }
}
