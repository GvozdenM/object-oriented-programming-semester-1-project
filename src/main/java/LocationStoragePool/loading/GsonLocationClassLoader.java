package LocationStoragePool.loading;

import com.google.gson.reflect.TypeToken;
import utils.ClassLoading.GsonClassLoader;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class GsonLocationClassLoader extends GsonClassLoader<ArrayList<LoadableLocation>>  {
    @Override
    public ArrayList<LoadableLocation> load(String json) {
        LoadableLocationWrapper loadableLocationWrapper = gson.fromJson(json, LoadableLocationWrapper.class);
        return new ArrayList<>(loadableLocationWrapper.locations);
    }
}
