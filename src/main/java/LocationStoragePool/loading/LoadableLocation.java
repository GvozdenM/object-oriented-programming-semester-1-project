package LocationStoragePool.loading;

import java.util.ArrayList;

public class LoadableLocation {
    private String name;
    private String description;
    private ArrayList<Double> coordinates;
    private boolean isUserDefined;
    private int stars;
    private ArrayList<String> characteristics;
    private String architect;

    public LoadableLocation(String name,
                            String description,
                            ArrayList<Double> coordinates,
                            boolean isUserDefined,
                            int stars,
                            ArrayList<String> characteristics,
                            String architect) {
        this.name = name;
        this.description = description;
        this.coordinates = coordinates;
        this.isUserDefined = isUserDefined;
        this.stars = stars;
        this.characteristics = characteristics;
        this.architect = architect;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<Double> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(ArrayList<Double> coordinates) {
        this.coordinates = coordinates;
    }

    public boolean isUserDefined() {
        return isUserDefined;
    }

    public void setUserDefined(boolean userDefined) {
        isUserDefined = userDefined;
    }

    public int getStars() {
        return stars;
    }

    public void setStars(int stars) {
        this.stars = stars;
    }

    public ArrayList<String> getCharacteristics() {
        return characteristics;
    }

    public void setCharacteristics(ArrayList<String> characteristics) {
        this.characteristics = characteristics;
    }

    public String getArchitect() {
        return architect;
    }

    public void setArchitect(String architect) {
        this.architect = architect;
    }
}
