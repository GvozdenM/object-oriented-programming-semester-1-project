package LocationStoragePool.loading.transformation;

import LocationStoragePool.loading.transformation.models.*;

public class DefaultLocationTransformChainFactory extends LocationTransformationHandlerFactory {
    @Override
    public LocationTransformationHandler createLocationTransformationHandler() {
        return new MonumentTransformationHandler(
                new HotelTransformationHandler(
                        new RestaurantTransformationHandler(
                                new DefaultLocationTransformationHandler(
                                        null
                                )
                        )
                )
        );
    }
}
