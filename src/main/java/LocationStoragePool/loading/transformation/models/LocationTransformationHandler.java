package LocationStoragePool.loading.transformation.models;

import LocationStoragePool.DataModel;
import LocationStoragePool.loading.LoadableLocation;

public abstract class LocationTransformationHandler {
    protected LocationTransformationHandler successor;

    public LocationTransformationHandler(LocationTransformationHandler successor) {
        this.successor = successor;
    }

    public boolean handleRequest(LoadableLocation loadedLocation, DataModel modelToAddTo) {
        if(this.passesLocationTypeTest(loadedLocation)) {
            this.createAndAdd(loadedLocation, modelToAddTo);
            return true;
        }

        else {
            return this.handoverToSuccessor(loadedLocation, modelToAddTo);
        }
    }

    private boolean hasSuccessor() {
        return null != successor;
    }

    private boolean handoverToSuccessor(LoadableLocation loadedLocation, DataModel modelToAddTo) {
        if(this.hasSuccessor()) {
            return this.successor.handleRequest(loadedLocation, modelToAddTo);
        }
        else {
            return false;
        }
    }

    protected abstract void createAndAdd(LoadableLocation loadedLocation, DataModel modelToAddTo);

    protected abstract boolean passesLocationTypeTest(LoadableLocation loadableLocation);
}
