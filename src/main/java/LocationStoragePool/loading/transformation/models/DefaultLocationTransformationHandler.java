package LocationStoragePool.loading.transformation.models;

import LocationStoragePool.DataModel;
import LocationStoragePool.loading.LoadableLocation;
import LocationStoragePool.loading.builders.AbstractLocationBuilder;
import LocationStoragePool.loading.builders.LocationBuilder;

public class DefaultLocationTransformationHandler extends LocationTransformationHandler {

    public DefaultLocationTransformationHandler(LocationTransformationHandler successor) {
        super(successor);
    }

    @Override
    protected void createAndAdd(LoadableLocation loadedLocation, DataModel modelToAddTo) {
        AbstractLocationBuilder builder = new LocationBuilder(
                loadedLocation.getName(),
                loadedLocation.getDescription(),
                loadedLocation.getCoordinates(),
                false
        );

        modelToAddTo.addLocation(builder.build());
    }

    @Override
    protected boolean passesLocationTypeTest(LoadableLocation loadableLocation) {
        return null != loadableLocation.getName()
                && null != loadableLocation.getCoordinates()
                && null != loadableLocation.getDescription();
    }
}
