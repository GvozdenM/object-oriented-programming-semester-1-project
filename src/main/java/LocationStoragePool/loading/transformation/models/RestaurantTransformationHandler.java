package LocationStoragePool.loading.transformation.models;

import LocationStoragePool.DataModel;
import LocationStoragePool.loading.LoadableLocation;
import LocationStoragePool.loading.builders.AbstractLocationBuilder;
import LocationStoragePool.loading.builders.LocationBuilder;

public class RestaurantTransformationHandler extends LocationTransformationHandler {
    public RestaurantTransformationHandler(LocationTransformationHandler successor) {
        super(successor);
    }

    @Override
    protected void createAndAdd(LoadableLocation loadedLocation, DataModel modelToAddTo) {

        AbstractLocationBuilder builder = new LocationBuilder(
                loadedLocation.getName(),
                loadedLocation.getDescription(),
                loadedLocation.getCoordinates(),
                false,
                loadedLocation.getCharacteristics()
        );

        modelToAddTo.addLocation(builder.build());
    }

    @Override
    protected boolean passesLocationTypeTest(LoadableLocation loadableLocation) {
        return null != loadableLocation.getCharacteristics();
    }
}
