package LocationStoragePool.loading.transformation.models;

import LocationStoragePool.DataModel;
import LocationStoragePool.loading.LoadableLocation;
import LocationStoragePool.loading.builders.AbstractLocationBuilder;
import LocationStoragePool.loading.builders.LocationBuilder;

public class HotelTransformationHandler extends LocationTransformationHandler {
    public HotelTransformationHandler(LocationTransformationHandler successor) {
        super(successor);
    }

    @Override
    protected void createAndAdd(LoadableLocation loadedLocation, DataModel modelToAddTo) {
        AbstractLocationBuilder builder = new LocationBuilder(
                loadedLocation.getName(),
                loadedLocation.getDescription(),
                loadedLocation.getCoordinates(),
                false,
                loadedLocation.getStars()
        );

        modelToAddTo.addLocation(builder.build());
    }

    @Override
    protected boolean passesLocationTypeTest(LoadableLocation loadableLocation) {
        return 0 != loadableLocation.getStars();
    }
}
