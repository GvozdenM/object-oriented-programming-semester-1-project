package LocationStoragePool.loading.transformation;

import LocationStoragePool.loading.transformation.models.LocationTransformationHandler;

public abstract class LocationTransformationHandlerFactory {
    public abstract LocationTransformationHandler createLocationTransformationHandler();
}
