package LocationStoragePool.loading.transformation.factories;

import LocationStoragePool.loading.transformation.models.*;
import LocationStoragePool.loading.transformation.LocationTransformationHandlerFactory;

public class DefaultLocationTransformChainFactory extends LocationTransformationHandlerFactory {
    @Override
    public LocationTransformationHandler createLocationTransformationHandler() {
        return new MonumentTransformationHandler(
                new HotelTransformationHandler(
                        new RestaurantTransformationHandler(
                                new DefaultLocationTransformationHandler(
                                        null
                                )
                        )
                )
        );
    }
}
