package utils.ClassLoading;

public abstract class GsonClassLoaderFactory<T> {
    public abstract GsonClassLoader<T> createLoader();
}
