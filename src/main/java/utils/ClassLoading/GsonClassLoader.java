package utils.ClassLoading;

import com.google.gson.Gson;

public abstract class GsonClassLoader<T> {
    protected Gson gson;

    public GsonClassLoader() {
        this.gson = new Gson();
    }

    public abstract T load(String json);
}
