package utils.TmbUrlConstructionDirectors.Abstract;

import org.apache.http.client.utils.URIBuilder;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public abstract class TmbApiUrlDirector<Req> {
    protected final String apiHost = "api.tmb.cat";
    protected final String scheme = "https";
    protected final String pathBase = "v1";
    private final String apiKey = "49d46c6d32c82731104cb439cf636f26";
    private final String apiId = "335e0f98";

    protected List<String> pathParts;
    protected Map<String, String> queryParams;
    protected Map<String, String> filterParams;
    protected URIBuilder builder;

    public TmbApiUrlDirector(URIBuilder builder) {
        this.builder = builder;
        this.pathParts = new LinkedList<>();
        this.queryParams = new HashMap<>();
        this.filterParams = new HashMap<>();

        this.pathParts.add(pathBase);
        this.builder.addParameter("app_key", this.apiKey);
        this.builder.addParameter("app_id", this.apiId);
    }

    protected void addPathPart(String part) {
        this.pathParts.add(part);
    }

    protected void addQueryParam(String param, String value) {
        this.queryParams.put(param, value);
    }

    protected void addQueryFilter(String param, String value) {
        this.filterParams.put(param, value);
    }

    protected void configureBuilder() {
        this.builder
                .setScheme(this.scheme)
                .setHost(this.apiHost)
                .setPathSegments(this.pathParts);



        for(String paramName : this.queryParams.keySet()) {
            this.builder.addParameter(paramName, this.queryParams.get(paramName));
        }

        for(String filterName : this.filterParams.keySet()) {
            this.builder.addParameter(filterName, this.filterParams.get(filterName));
        }
    }

    public abstract void constructUrl(Req request);
}
