package utils.TmbUrlConstructionDirectors.Abstract;

import AbstractRouteMapping.TmbApiRouteLookupRequest;
import org.apache.http.client.utils.URIBuilder;

public class TmbApiRouteUrlDirector extends TmbApiUrlDirector<TmbApiRouteLookupRequest> {
    protected final String routePlannerBasePath = "planner";
    protected final String routePlannerPlanPath = "plan";
    protected final String routeOriginParameterKey = "fromPlace";
    protected final String routeDestinationParameterKey = "toPlace";
    protected final String routeTimeParameterKey = "time";
    protected final String routeMaxWalkDistanceParameterKey = "maxWalkDistance";

    public TmbApiRouteUrlDirector(URIBuilder builder) {
        super(builder);
        this.addPathPart(routePlannerBasePath);
        this.addPathPart(routePlannerPlanPath);
    }

    @Override
    public void constructUrl(TmbApiRouteLookupRequest request) {
        this.addQueryParam(this.routeOriginParameterKey, request.getFromPlace());
        this.addQueryParam(this.routeDestinationParameterKey, request.getToPlace());
        this.addQueryParam(this.routeTimeParameterKey, request.getTime());
        this.addQueryParam(this.routeMaxWalkDistanceParameterKey, Integer.toString(request.getMaxWalkingDistance()));
    }
}
