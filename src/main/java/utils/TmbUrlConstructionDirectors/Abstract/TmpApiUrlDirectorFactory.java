package utils.TmbUrlConstructionDirectors.Abstract;

import org.apache.http.client.utils.URIBuilder;

public abstract class TmpApiUrlDirectorFactory<T> {
    public abstract TmbApiUrlDirector<T> createUrlDirector(URIBuilder builder);
}
