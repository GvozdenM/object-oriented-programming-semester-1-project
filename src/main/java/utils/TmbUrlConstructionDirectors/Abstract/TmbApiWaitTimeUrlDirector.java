package utils.TmbUrlConstructionDirectors.Abstract;

import org.apache.http.client.utils.URIBuilder;

public class TmbApiWaitTimeUrlDirector extends TmbApiUrlDirector<String> {
    protected final String routePlannerBasePath = "ibus";
    protected final String routeStopsPath = "stops";

    public TmbApiWaitTimeUrlDirector(URIBuilder builder) {
        super(builder);
        this.addPathPart(this.routePlannerBasePath);
        this.addPathPart(this.routeStopsPath);
    }

    @Override
    public void constructUrl(String request) {
        this.addPathPart(request);
        this.configureBuilder();
    }
}
