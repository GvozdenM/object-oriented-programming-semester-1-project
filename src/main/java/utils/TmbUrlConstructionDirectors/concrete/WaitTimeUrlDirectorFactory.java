package utils.TmbUrlConstructionDirectors.concrete;

import org.apache.http.client.utils.URIBuilder;
import utils.TmbUrlConstructionDirectors.Abstract.TmbApiUrlDirector;
import utils.TmbUrlConstructionDirectors.Abstract.TmbApiWaitTimeUrlDirector;
import utils.TmbUrlConstructionDirectors.Abstract.TmpApiUrlDirectorFactory;

public class WaitTimeUrlDirectorFactory extends TmpApiUrlDirectorFactory<String> {
    @Override
    public TmbApiUrlDirector<String> createUrlDirector(URIBuilder builder) {
        return new TmbApiWaitTimeUrlDirector(builder);
    }
}
