package utils.TmbUrlConstructionDirectors.concrete;


import AbstractRouteMapping.TmbApiRouteLookupRequest;
import org.apache.http.client.utils.URIBuilder;
import utils.TmbUrlConstructionDirectors.Abstract.TmbApiRouteUrlDirector;
import utils.TmbUrlConstructionDirectors.Abstract.TmbApiUrlDirector;
import utils.TmbUrlConstructionDirectors.Abstract.TmpApiUrlDirectorFactory;

public class RouteLookupUrlDirectorFactory extends TmpApiUrlDirectorFactory<TmbApiRouteLookupRequest> {
    @Override
    public TmbApiUrlDirector<TmbApiRouteLookupRequest> createUrlDirector(URIBuilder builder) {
        return new TmbApiRouteUrlDirector(builder);
    }
}
