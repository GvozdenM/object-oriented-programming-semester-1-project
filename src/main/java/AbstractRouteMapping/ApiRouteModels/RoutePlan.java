package AbstractRouteMapping.ApiRouteModels;

import java.util.ArrayList;
public class RoutePlan {
    private long date;
    private ArrayList<RouteItinerary> itineraries;

    public long getDate() {
        return date;
    }

    public ArrayList<RouteItinerary> getItineraries() {
        return itineraries;
    }
}
