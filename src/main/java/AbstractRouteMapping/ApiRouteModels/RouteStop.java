package AbstractRouteMapping.ApiRouteModels;

import com.google.gson.annotations.SerializedName;

public class RouteStop {
    private String name;
    private String stopId;
    @SerializedName("lon")
    private double longitude;
    @SerializedName("lat")
    private double latitude;
    private long arrival;
    private long departure;
    private int stopIndex;
    private int stopSequence;
    private String vertexType;

    public String getName() {
        return name;
    }

    public String getStopId() {
        return stopId;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public long getArrival() {
        return arrival;
    }

    public long getDeparture() {
        return departure;
    }

    public int getStopIndex() {
        return stopIndex;
    }

    public int getStopSequence() {
        return stopSequence;
    }

    public String getVertexType() {
        return vertexType;
    }
}
