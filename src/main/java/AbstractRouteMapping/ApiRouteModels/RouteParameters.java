package AbstractRouteMapping.ApiRouteModels;

public class RouteParameters {
    private String date;
    private String walkSpeed;
    private String walkReluctance;
    private String softWalkOverageRate;
    private String fromPlace;
    private String maxHours;
    private String softWalkPenalty;
    private String transferPenalty;
    private String waitReluctance;
    private String maxWalkDistance;
    private String maxTransfers;
    private String numItineraries;
    private String waitATBeginningFactor;
    private String preferredAgencies;
    private String walkBoardCost;
    private String toPlace;
    private String time;
    private String maxTransferWalkDistance;

    public String getDate() {
        return date;
    }

    public String getWalkSpeed() {
        return walkSpeed;
    }

    public String getWalkReluctance() {
        return walkReluctance;
    }

    public String getSoftWalkOverageRate() {
        return softWalkOverageRate;
    }

    public String getFromPlace() {
        return fromPlace;
    }

    public String getMaxHours() {
        return maxHours;
    }

    public String getSoftWalkPenalty() {
        return softWalkPenalty;
    }

    public String getTransferPenalty() {
        return transferPenalty;
    }

    public String getWaitReluctance() {
        return waitReluctance;
    }

    public String getMaxWalkDistance() {
        return maxWalkDistance;
    }

    public String getMaxTransfers() {
        return maxTransfers;
    }

    public String getNumItineraries() {
        return numItineraries;
    }

    public String getWaitATBeginningFactor() {
        return waitATBeginningFactor;
    }

    public String getPreferredAgencies() {
        return preferredAgencies;
    }

    public String getWalkBoardCost() {
        return walkBoardCost;
    }

    public String getToPlace() {
        return toPlace;
    }

    public String getTime() {
        return time;
    }

    public String getMaxTransferWalkDistance() {
        return maxTransferWalkDistance;
    }
}
