package AbstractRouteMapping.ApiRouteModels;

import com.google.gson.annotations.SerializedName;

public class RouteLocation {
    private String name;
    @SerializedName("lon")
    private double longitude;
    @SerializedName("lat")
    private double latitude;
    private long departure;
    private String orig;
    private String stopCode;

    public String getStopCode() {
        return stopCode;
    }

    public String getName() {
        return name;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public long getDeparture() {
        return departure;
    }

    public String getOrig() {
        return orig;
    }
}
