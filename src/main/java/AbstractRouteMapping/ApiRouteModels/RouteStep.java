package AbstractRouteMapping.ApiRouteModels;

public class RouteStep {
    private double distance;
    private String relativeDirection;
    private String streetName;
    private boolean stayOn;

    public double getDistance() {
        return distance;
    }

    public String getRelativeDirection() {
        return relativeDirection;
    }

    public String getStreetName() {
        return streetName;
    }

    public boolean isStayOn() {
        return stayOn;
    }
}
