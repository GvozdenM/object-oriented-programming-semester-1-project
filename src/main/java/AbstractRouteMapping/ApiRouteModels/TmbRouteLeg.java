package AbstractRouteMapping.ApiRouteModels;

public class TmbRouteLeg {
    private long startTime;
    private long endTime;
    private int departureDelay;
    private int arrivalDelay;
    private boolean realTime;
    private double distance;
    private boolean pathway;
    private String mode;
    private long agencyTimeZoneOffset;
    private boolean interlineWithPreviousLeg;
    private RouteLocation from;
    private RouteLocation to;
    private int duration;
    private boolean transitLeg;
    private RouteStep steps;
    private String route;
    private String routeShortName;
    private String routeLongName;
    private RouteStop intermediateStops;

    public long getStartTime() {
        return startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public int getDepartureDelay() {
        return departureDelay;
    }

    public int getArrivalDelay() {
        return arrivalDelay;
    }

    public boolean isRealTime() {
        return realTime;
    }

    public double getDistance() {
        return distance;
    }

    public boolean isPathway() {
        return pathway;
    }

    public String getMode() {
        return mode;
    }

    public long getAgencyTimeZoneOffset() {
        return agencyTimeZoneOffset;
    }

    public boolean isInterlineWithPreviousLeg() {
        return interlineWithPreviousLeg;
    }

    public RouteLocation getFrom() {
        return from;
    }

    public RouteLocation getTo() {
        return to;
    }

    public int getDuration() {
        return duration;
    }

    public boolean isTransitLeg() {
        return transitLeg;
    }

    public RouteStep getSteps() {
        return steps;
    }

    public String getRoute() {
        return route;
    }

    public String getRouteShortName() {
        return routeShortName;
    }

    public String getRouteLongName() {
        return routeLongName;
    }

    public RouteStop getIntermediateStops() {
        return intermediateStops;
    }
}
