package AbstractRouteMapping.ApiRouteModels;


import java.util.List;

public class RouteItinerary {
    private int duration;
    private long startTime;
    private long endTime;
    private int walkTime;
    private int transitTime;
    private double walkDistance;
    private boolean walkLimitExceeded;
    private int elevationLost;
    private int elevationGained;
    private int transfers;
    private List<TmbRouteLeg> legs;
    private boolean tooSloped;

    public int getDuration() {
        return duration;
    }

    public long getStartTime() {
        return startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public int getWalkTime() {
        return walkTime;
    }

    public int getTransitTime() {
        return transitTime;
    }

    public double getWalkDistance() {
        return walkDistance;
    }

    public boolean isWalkLimitExceeded() {
        return walkLimitExceeded;
    }

    public int getElevationLost() {
        return elevationLost;
    }

    public int getElevationGained() {
        return elevationGained;
    }

    public int getTransfers() {
        return transfers;
    }

    public List<TmbRouteLeg> getLegs() {
        return legs;
    }

    public boolean isTooSloped() {
        return tooSloped;
    }
}
