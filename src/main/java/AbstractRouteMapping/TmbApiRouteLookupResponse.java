package AbstractRouteMapping;

import AbstractRouteMapping.ApiRouteModels.RouteParameters;
import AbstractRouteMapping.ApiRouteModels.RoutePlan;

public class TmbApiRouteLookupResponse {
    private RouteParameters requestParameters;
    private RoutePlan plan;

    public RouteParameters getRequestParameters() {
        return requestParameters;
    }

    public RoutePlan getPlan() {
        return plan;
    }
}
