package AbstractRouteMapping;

public interface TmbApiRouteRequester {
    TmbApiRouteLookupResponse getRoutes(TmbApiRouteLookupRequest request);
}
