package AbstractRouteMapping;

public class TmbApiRouteLookupRequest {
    private String fromPlace;
    private String toPlace;
    private String time;
    private int maxWalkingDistance;

    public TmbApiRouteLookupRequest(String fromPlace, String toPlace, String time, int maxWalkingDistance) {
        this.fromPlace = fromPlace;
        this.toPlace = toPlace;
        this.time = time;
        this.maxWalkingDistance = maxWalkingDistance;
    }

    public String getFromPlace() {
        return fromPlace;
    }

    public void setFromPlace(String fromPlace) {
        this.fromPlace = fromPlace;
    }

    public String getToPlace() {
        return toPlace;
    }

    public void setToPlace(String toPlace) {
        this.toPlace = toPlace;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getMaxWalkingDistance() {
        return maxWalkingDistance;
    }

    public void setMaxWalkingDistance(int maxWalkingDistance) {
        this.maxWalkingDistance = maxWalkingDistance;
    }
}
