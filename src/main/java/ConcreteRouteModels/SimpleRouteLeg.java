package ConcreteRouteModels;

import AbstractRouteModels.RouteLeg;

import java.util.LinkedHashMap;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class SimpleRouteLeg extends RouteLeg<LinkedHashMap<String, String>> {

    public SimpleRouteLeg(
            int duration,
            double distance,
            boolean isTransitLeg,
            Map<String, String> metadata) {

        super(
                duration,
                distance,
                isTransitLeg,
                new LinkedHashMap<>(metadata)
        );
    }

    @Override
    public List<String> getMetadataList() {
        return new LinkedList<>(this.metadata.values());
    }
}
