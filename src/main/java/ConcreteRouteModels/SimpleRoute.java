package ConcreteRouteModels;

import AbstractLocationModels.PrimitiveLocation;
import AbstractRouteModels.Route;

import java.util.LinkedList;
import java.util.List;

public class SimpleRoute extends Route<SimpleRouteLeg> {
    public SimpleRoute() {
        super();
    }

    public SimpleRoute(
            PrimitiveLocation origin,
            PrimitiveLocation destination,
            int duration,
            int walkingDistance,
            List<SimpleRouteLeg> legs) {

        super(
                origin,
                destination,
                duration,
                walkingDistance,
                new LinkedList<>(legs)
        );
    }
}
