package AbstractUserStorage;

public interface UserBaseDataGateway extends UserNameGateway, UserEmailGateway, UserBirthYearGateway{
}
