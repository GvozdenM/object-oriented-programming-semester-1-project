package AbstractUserStorage;

public interface UserLocationSearchRegistrationGateway {
    void registerLocationSearch(String locationName);
}
