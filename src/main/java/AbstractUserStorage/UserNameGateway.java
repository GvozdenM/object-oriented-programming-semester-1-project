package AbstractUserStorage;

public interface UserNameGateway {
    String getUserName();
}
