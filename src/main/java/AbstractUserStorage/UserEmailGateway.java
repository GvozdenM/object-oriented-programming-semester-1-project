package AbstractUserStorage;

public interface UserEmailGateway {
    String getUserEmail();
}
