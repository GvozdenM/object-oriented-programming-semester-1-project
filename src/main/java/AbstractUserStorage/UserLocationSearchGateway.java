package AbstractUserStorage;

import java.util.List;

public interface UserLocationSearchGateway {
    List<String> getSearchedLocations();
}
