package AbstractUserStorage;

public interface UserFavoriteLocationRegistrationGateway {
    void registerFavoriteLocation(String locationName) throws DuplicateFavoriteLocationException;
}