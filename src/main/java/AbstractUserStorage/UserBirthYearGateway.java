package AbstractUserStorage;

public interface UserBirthYearGateway {
    String getUserBirthYear();
}
