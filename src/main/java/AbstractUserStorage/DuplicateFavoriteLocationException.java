package AbstractUserStorage;

public class DuplicateFavoriteLocationException extends Exception {
    private final String locationName;

    public DuplicateFavoriteLocationException(String locationName) {
        this.locationName = locationName;
    }

    @Override
    public String
    toString() {
        return "DuplicateFavoriteLocationException{" +
                "locationName='" + locationName + '\'' +
                '}';
    }
}
