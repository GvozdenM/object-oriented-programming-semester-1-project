package AbstractUserStorage;

import AbstractRouteLookupInteractors.RouteLookupRequest;

public interface UserRouteLookupRegistrationGateway {
    void registerRoute(RouteLookupRequest route);
}
