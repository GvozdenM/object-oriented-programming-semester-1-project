package AbstractUserStorage;

public interface UserFavoriteLocationIncludedGateway {
    boolean hasFavoriteLocation(String locationName);
}
