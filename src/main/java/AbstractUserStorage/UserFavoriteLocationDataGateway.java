package AbstractUserStorage;


import java.util.Set;

public interface UserFavoriteLocationDataGateway {
    Set<String> getFavoriteLocationNames();
}
