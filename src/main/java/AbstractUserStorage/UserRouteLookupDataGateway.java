package AbstractUserStorage;

import AbstractRouteLookupInteractors.RouteLookupRequest;

import java.util.List;

public interface UserRouteLookupDataGateway {
    List<RouteLookupRequest> getUserRoutes();
}
