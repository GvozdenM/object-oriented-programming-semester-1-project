package ConcreteLocationSearchHistoryController;

import AbstractLocationSearchHistoryController.AbstractLocationSearchHistoryController;
import AbstractLocationSearchHistoryPresenter.AbstractLocationSearchHistoryPresenter;
import AbstractLocationSearchHistoryPresenter.LocationSearchHistoryPresenterFactory;
import AbstractUserStorage.UserLocationSearchGateway;

public class LocationSearchHistoryController implements AbstractLocationSearchHistoryController {
    private LocationSearchHistoryPresenterFactory presenterFactory;

    public LocationSearchHistoryController(LocationSearchHistoryPresenterFactory presenterFactory) {
        this.presenterFactory = presenterFactory;
    }

    @Override
    public void process(UserLocationSearchGateway searchGateway) {
        AbstractLocationSearchHistoryPresenter presenter = this.presenterFactory.create();

        presenter.presentHistory(searchGateway.getSearchedLocations());

    }
}
