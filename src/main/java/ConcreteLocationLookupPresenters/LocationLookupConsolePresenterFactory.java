package ConcreteLocationLookupPresenters;

import AbstractLocationLookupPresenters.LookedUpLocationPresenter;
import AbstractLocationLookupPresenters.LookedUpLocationPresenterFactory;
import ConcreteLocationLookupViews.LocationLookupConsoleViewFactory;

public class LocationLookupConsolePresenterFactory extends LookedUpLocationPresenterFactory {
    @Override
    public LookedUpLocationPresenter createPresenter() {
        return new LocationLookupScreenPresenter(
                new LocationLookupConsoleViewFactory()
        );
    }
}
