package ConcreteLocationLookupPresenters.viewInterfaces;

import AbstractLocationLookupPresenters.LocationLookupViewModel;

public interface LocationLookupView {
    void displayLocation(LocationLookupViewModel location);

    void displayNoLocationFound(String noLocationFoundMessage);
}
