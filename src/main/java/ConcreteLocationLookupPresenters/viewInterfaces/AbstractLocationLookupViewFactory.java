package ConcreteLocationLookupPresenters.viewInterfaces;

public abstract class AbstractLocationLookupViewFactory {
    public abstract LocationLookupView createView();
}
