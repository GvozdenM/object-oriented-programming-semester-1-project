package ConcreteLocationLookupPresenters;

import AbstractLocationLookupInteractors.LookedUpLocation;
import AbstractLocationLookupPresenters.LocationLookupViewModel;
import AbstractLocationLookupPresenters.LookedUpLocationPresenter;
import ConcreteLocationLookupPresenters.viewInterfaces.AbstractLocationLookupViewFactory;
import ConcreteLocationLookupPresenters.viewInterfaces.LocationLookupView;
import Main.Main;

import java.util.Collection;
import java.util.Set;

public class LocationLookupScreenPresenter implements LookedUpLocationPresenter {
    private LocationLookupView view;
    private Set<String> favoriteLocations;

    public LocationLookupScreenPresenter(AbstractLocationLookupViewFactory viewFactory) {
        this.view = viewFactory.createView();
        this.favoriteLocations = Main.favoriteLocationDataGateway.getFavoriteLocationNames();

    }

    @Override
    public void presentFoundLocations(Collection<LookedUpLocation> locations) {
        for(LookedUpLocation location : locations) {
            presentFoundLocation(location);
        }
    }

    private void presentFoundLocation(LookedUpLocation location) {
        boolean isFavoriteLocation = this.favoriteLocations.contains(location.getLocationName());

        LocationLookupViewModel viewModel = new LocationLookupViewModel(
                location.getLocationName(),
                location.getLocationDescription(),
                String.valueOf(location.getLatitude()),
                String.valueOf(location.getLongitude()),
                isFavoriteLocation,
                location.getMetadata()
        );

        this.view.displayLocation(viewModel);
    }

    @Override
    public void presentNoLocationsFound() {
        this.view.displayNoLocationFound(
                "We could not find any locations!"
        );
    }
}
