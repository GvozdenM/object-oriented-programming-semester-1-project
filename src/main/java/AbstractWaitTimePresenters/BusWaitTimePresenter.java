package AbstractWaitTimePresenters;

import AbstractWaitTimeInteractor.WaitTimeResponse;

public interface BusWaitTimePresenter {
    void presentWaitTimes(WaitTimeResponse waitTimeResponse);
    void presentInteractorError(WaitTimeResponse waitTimeResponse);
}
