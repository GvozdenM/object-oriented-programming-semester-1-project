package AbstractWaitTimePresenters;

import java.util.LinkedHashMap;
import java.util.Map;

public class BusWaitTimeViewModel {
    private Map<String, String> waitTimes;
    private boolean isFavoriteStop;

    public BusWaitTimeViewModel(boolean isFavoriteStop) {
        this.isFavoriteStop = isFavoriteStop;
    }

    public BusWaitTimeViewModel(Map<String, String> lineNamesAndWaitTimes, boolean isFavoriteStop) {
        this.waitTimes = new LinkedHashMap<>(lineNamesAndWaitTimes);
        this.isFavoriteStop = isFavoriteStop;
    }

    public boolean isFavoriteStop() {
        return isFavoriteStop;
    }

    public void setFavoriteStop(boolean favoriteStop) {
        isFavoriteStop = favoriteStop;
    }

    public Map<String, String> getWaitTimes() {
        return waitTimes;
    }

    public void setWaitTimes(Map<String, String> waitTimes) {
        this.waitTimes = waitTimes;
    }
}
