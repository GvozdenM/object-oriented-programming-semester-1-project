package AbstractWaitTimePresenters;

public abstract class WaitTimePresenterFactory {
    public abstract BusWaitTimePresenter createWaitTimePresenter();
}
