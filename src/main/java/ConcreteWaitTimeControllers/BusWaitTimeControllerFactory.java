package ConcreteWaitTimeControllers;

import Main.*;

public class BusWaitTimeControllerFactory extends AbstractControllerFactory<String> {
    @Override
    public ControllerI<String> createController() {
        return new BusWaitTimeController(
                Main.waitTimeInteractorFactory,
                Main.waitTimePresenterFactory
        );
    }
}
