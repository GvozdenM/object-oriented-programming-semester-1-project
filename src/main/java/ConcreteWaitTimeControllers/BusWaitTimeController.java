package ConcreteWaitTimeControllers;

import AbstractWaitTimePresenters.BusWaitTimePresenter;
import AbstractWaitTimeInteractor.WaitTimeInteractorFactory;
import AbstractWaitTimePresenters.WaitTimePresenterFactory;
import AbstractWaitTimeInteractor.WaitTimeRequester;
import AbstractWaitTimeInteractor.WaitTimeResponse;

import java.io.IOException;
import java.net.URISyntaxException;

public class BusWaitTimeController implements AbstractWaitTimeControllers.BusWaitTimeController {
    private WaitTimeRequester interactor;
    private BusWaitTimePresenter presenter;

    public BusWaitTimeController(WaitTimeInteractorFactory interactorFactory, WaitTimePresenterFactory presenterFactory) {
        this.interactor = interactorFactory.createWaitTimeInteractor();
        this.presenter = presenterFactory.createWaitTimePresenter();
    }

    @Override
    public boolean process(String stopCode) throws IOException, URISyntaxException {
        WaitTimeResponse response = this.interactor.getWaitTime(stopCode);

        if(response.isSuccess()) {
            this.presenter.presentWaitTimes(response);
            return true;
        }
        else {
            this.presenter.presentInteractorError(response);
            return false;
        }
    }
}
