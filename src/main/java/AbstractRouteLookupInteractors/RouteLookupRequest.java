package AbstractRouteLookupInteractors;

import java.util.Date;

public class RouteLookupRequest {
    private double originLatitude;
    private double originLongitude;
    private double destinationLatitude;
    private double destinationLongitude;
    private Date time;
    private boolean isDepartureTime;
    private int maxWalkingDistance;
    private String sortingType;

    public RouteLookupRequest(double originLatitude,
                              double originLongitude,
                              double destinationLatitude,
                              double destinationLongitude,
                              Date time,
                              boolean isDepartureTime,
                              int maxWalkingDistance,
                              String sortingType) {
        this.originLatitude = originLatitude;
        this.originLongitude = originLongitude;
        this.destinationLatitude = destinationLatitude;
        this.destinationLongitude = destinationLongitude;
        this.time = (Date) time.clone();
        this.isDepartureTime = isDepartureTime;
        this.maxWalkingDistance = maxWalkingDistance;
        this.sortingType = sortingType;
    }

    public double getOriginLatitude() {
        return originLatitude;
    }

    public void setOriginLatitude(double originLatitude) {
        this.originLatitude = originLatitude;
    }

    public double getOriginLongitude() {
        return originLongitude;
    }

    public void setOriginLongitude(double originLongitude) {
        this.originLongitude = originLongitude;
    }

    public double getDestinationLatitude() {
        return destinationLatitude;
    }

    public void setDestinationLatitude(double destinationLatitude) {
        this.destinationLatitude = destinationLatitude;
    }

    public double getDestinationLongitude() {
        return destinationLongitude;
    }

    public void setDestinationLongitude(double destinationLongitude) {
        this.destinationLongitude = destinationLongitude;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public boolean isDepartureTime() {
        return isDepartureTime;
    }

    public void setDepartureTime(boolean departureTime) {
        isDepartureTime = departureTime;
    }

    public int getMaxWalkingDistance() {
        return maxWalkingDistance;
    }

    public void setMaxWalkingDistance(int maxWalkingDistance) {
        this.maxWalkingDistance = maxWalkingDistance;
    }

    public String getSortingType() {
        return sortingType;
    }

    public void setSortingType(String sortingType) {
        this.sortingType = sortingType;
    }
}
