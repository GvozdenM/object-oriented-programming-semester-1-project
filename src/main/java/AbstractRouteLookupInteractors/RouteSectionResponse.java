package AbstractRouteLookupInteractors;

import java.util.Map;

public abstract class RouteSectionResponse {
    private int sectionTotalTimeMinutes;
    private boolean usesTransit;
    private Map<String, String> metadata;

    public RouteSectionResponse(int sectionTotalTimeMinutes, boolean usesTransit, Map<String, String> metadata) {
        this.sectionTotalTimeMinutes = sectionTotalTimeMinutes;
        this.usesTransit = usesTransit;
        this.metadata = metadata;
    }

    public int getSectionTotalTimeMinutes() {
        return sectionTotalTimeMinutes;
    }

    public void setSectionTotalTimeMinutes(int sectionTotalTimeMinutes) {
        this.sectionTotalTimeMinutes = sectionTotalTimeMinutes;
    }

    public boolean isUsesTransit() {
        return usesTransit;
    }

    public void setUsesTransit(boolean usesTransit) {
        this.usesTransit = usesTransit;
    }

    public Map<String, String> getMetadata() {
        return metadata;
    }

    public void setMetadata(Map<String, String> metadata) {
        this.metadata = metadata;
    }
}
