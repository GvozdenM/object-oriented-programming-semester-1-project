package AbstractRouteLookupInteractors;

import java.util.List;

public abstract class RouteLookupResponse {
    private boolean success;
    private String error;

    private String originName;
    private String destinationName;
    private int totalTimeMinutes;
    private List<RouteSectionResponse> sectionList;

    public RouteLookupResponse(boolean success,
                               String error,
                               String originName,
                               String destinationName,
                               int totalTimeMinutes,
                               List<RouteSectionResponse> sectionList) {
        this.success = success;
        this.error = error;
        this.originName = originName;
        this.destinationName = destinationName;
        this.totalTimeMinutes = totalTimeMinutes;
        this.sectionList = sectionList;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getOriginName() {
        return originName;
    }

    public void setOriginName(String originName) {
        this.originName = originName;
    }

    public String getDestinationName() {
        return destinationName;
    }

    public void setDestinationName(String destinationName) {
        this.destinationName = destinationName;
    }

    public int getTotalTimeMinutes() {
        return totalTimeMinutes;
    }

    public void setTotalTimeMinutes(int totalTimeMinutes) {
        this.totalTimeMinutes = totalTimeMinutes;
    }

    public List<RouteSectionResponse> getSectionList() {
        return sectionList;
    }

    public void setSectionList(List<RouteSectionResponse> sectionList) {
        this.sectionList = sectionList;
    }
}
