package AbstractRouteLookupInteractors;

public interface RouteLookupRequester<Req, Res> {
    Res getRoute(Req routeLookupRequest);
}
