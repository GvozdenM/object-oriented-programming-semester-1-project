package ConcreteLocationLookupInteractors;

import AbstractLocationMapping.DuplicateLocationException;

import java.io.IOException;
import java.util.List;


public interface LocationDataGateway<Loc> {
    List<Loc> getAllLocations() throws IOException;
    List<Loc> getUserDefinedLocations() throws IOException;
    Loc getLocation(String name) throws IOException;
    void addLocation(Loc location) throws DuplicateLocationException, IOException;
}
