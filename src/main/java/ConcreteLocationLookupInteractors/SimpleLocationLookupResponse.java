package ConcreteLocationLookupInteractors;

import AbstractLocationLookupInteractors.LocationLookupResponse;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;

public class SimpleLocationLookupResponse extends LocationLookupResponse {
    public SimpleLocationLookupResponse() {
        super(new LinkedList<>());
    }
}
