package ConcreteLocationLookupInteractors;

import AbstractLocationLookupInteractors.LookedUpLocation;

import java.util.LinkedHashMap;
import java.util.Map;

public class SimpleLookedUpLocation extends LookedUpLocation {
    public SimpleLookedUpLocation(String locationName,
                                  String locationDescription,
                                  Map<String, String> metadata,
                                  double latitude,
                                  double longitude) {
        super(
                locationName,
                locationDescription,
                new LinkedHashMap<>(metadata),
                latitude,
                longitude
        );
    }
}
