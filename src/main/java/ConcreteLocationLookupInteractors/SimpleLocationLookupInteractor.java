package ConcreteLocationLookupInteractors;

import AbstractLocationLookupInteractors.LocationLookupRequester;
import AbstractLocationLookupInteractors.LocationLookupResponse;
import AbstractLocationMapping.UnifiedLocationDataBank;
import AbstractLocationMapping.UnifiedLocationDataGatewayFactory;
import AbstractLocationModels.DescribedLocation;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;


public class SimpleLocationLookupInteractor implements LocationLookupRequester<LocationLookupResponse> {
    private UnifiedLocationDataGatewayFactory<DescribedLocation> unifiedLocationDataGatewayFactory;

    public SimpleLocationLookupInteractor(UnifiedLocationDataGatewayFactory<DescribedLocation> unifiedLocationDataGatewayFactory) {
        this.unifiedLocationDataGatewayFactory = unifiedLocationDataGatewayFactory;
    }

    @Override
    public SimpleLocationLookupResponse getLocation(String locationName) throws IOException {
        LocationDataGateway<DescribedLocation> gateway =
                this.unifiedLocationDataGatewayFactory.createUnifiedLocationDataGateway();

        DescribedLocation location = gateway.getLocation(locationName);

        SimpleLocationLookupResponse response = new SimpleLocationLookupResponse();

        if(null != location) {

            response.setAnyExist(true);
            response.setNumFound(1);
            response.addLocation(
                    new SimpleLookedUpLocation(
                            location.getName(),
                            location.getLongDescription(),
                            location.getMetadata(),
                            location.getLatitude(),
                            location.getLongitude()
                    )
            );

        }

        else {
            response.setAnyExist(false);
            response.setNumFound(0);
        }

        return response;
    }
}
