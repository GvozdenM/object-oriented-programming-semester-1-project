package ConcreteLocationLookupInteractors;

import AbstractLocationLookupInteractors.LocationLookupRequester;
import AbstractLocationLookupInteractors.LocationLookupRequesterFactory;
import AbstractLocationLookupInteractors.LocationLookupResponse;
import ConcreteLocationMapping.ConcreteLocationMapperFactory;

public class SimpleLocationLookupInteractorFactory extends LocationLookupRequesterFactory<LocationLookupResponse> {
    @Override
    public LocationLookupRequester<LocationLookupResponse> createInteractor() {
        return new SimpleLocationLookupInteractor(
                new ConcreteLocationMapperFactory()
        );
    }
}
