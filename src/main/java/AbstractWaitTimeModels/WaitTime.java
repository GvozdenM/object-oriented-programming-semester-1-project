package AbstractWaitTimeModels;

public class WaitTime {
    private String lineName;
    private String routeId;
    private int waitTimeMinutes;
    private int waitTimeSeconds;

    public WaitTime() {
    }

    public WaitTime(String lineName, String routeId, int waitTimeMinutes, int waitTimeSeconds) {
        this.lineName = lineName;
        this.routeId = routeId;
        this.waitTimeMinutes = waitTimeMinutes;
        this.waitTimeSeconds = waitTimeSeconds;
    }

    public String getLineName() {
        return lineName;
    }

    public void setLineName(String lineName) {
        this.lineName = lineName;
    }

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public int getWaitTimeMinutes() {
        return waitTimeMinutes;
    }

    public void setWaitTimeMinutes(int waitTimeMinutes) {
        this.waitTimeMinutes = waitTimeMinutes;
    }

    public int getWaitTimeSeconds() {
        return waitTimeSeconds;
    }

    public void setWaitTimeSeconds(int waitTimeSeconds) {
        this.waitTimeSeconds = waitTimeSeconds;
    }
}
