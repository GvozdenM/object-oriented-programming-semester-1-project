package ConcreteLocationSearchHistoryPresenter;

import AbstractLocationSearchHistoryPresenter.AbstractLocationSearchHistoryPresenter;
import AbstractLocationSearchHistoryPresenter.LocationSearchHistoryPresenterFactory;
import ConcreteLocationSearchHistoryView.LocationSearchHistoryViewFactory;

public class ConcreteLocationSearchHistoryPresenterFactory extends LocationSearchHistoryPresenterFactory {
    @Override
    public AbstractLocationSearchHistoryPresenter create() {
        return new ConcreteLocationSearchHistoryPresenter(new LocationSearchHistoryViewFactory());
    }
}
