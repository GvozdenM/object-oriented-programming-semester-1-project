package ConcreteLocationSearchHistoryPresenter;

import AbstractLocationSearchHistoryPresenter.*;

import java.util.Collection;

public class ConcreteLocationSearchHistoryPresenter extends AbstractLocationSearchHistoryPresenter {
    AbstractLocationSearchHistoryViewFactory viewFactory;

    ConcreteLocationSearchHistoryPresenter(AbstractLocationSearchHistoryViewFactory viewFactory) {
        this.viewFactory = viewFactory;
    }

    @Override
    public void presentHistory(Collection<String> searchedLocations) {
        AbstractLocationSerachHistoryView view = viewFactory.createView();

        if(0 != searchedLocations.size()) {
            view.displayHistory(searchedLocations);
        }
        else {
            view.displayNoHistory();
        }
    }
}
