package AbstractRouteLookupControllers;

public interface RouteLookupController {
    void getRoute(String originLatitude,
             String originLongitude,
             String destinationLatitude,
             String destinationLongitude,
             String time,
             String isDepartureOrArrivalTime
    );
}
