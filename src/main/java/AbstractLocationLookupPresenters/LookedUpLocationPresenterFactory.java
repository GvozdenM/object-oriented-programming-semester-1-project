package AbstractLocationLookupPresenters;

public abstract class LookedUpLocationPresenterFactory {
    public abstract LookedUpLocationPresenter createPresenter();
}
