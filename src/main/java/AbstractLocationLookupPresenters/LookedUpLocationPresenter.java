package AbstractLocationLookupPresenters;

import AbstractLocationLookupInteractors.LookedUpLocation;

import java.util.Collection;

public interface LookedUpLocationPresenter {
    void presentFoundLocations(Collection<LookedUpLocation> locations);


    void presentNoLocationsFound();
}
