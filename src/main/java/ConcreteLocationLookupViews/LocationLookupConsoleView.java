package ConcreteLocationLookupViews;

import ConcreteLocationLookupPresenters.viewInterfaces.LocationLookupView;
import AbstractLocationLookupPresenters.LocationLookupViewModel;

public class LocationLookupConsoleView implements LocationLookupView {

    @Override
    public void displayLocation(LocationLookupViewModel location) {
        StringBuilder builder = new StringBuilder();

        builder
                .append("Position : ")
                .append("\t")
                .append("latitude: ")
                .append(location.getLongitude())
                .append(" ")
                .append("longitude: ")
                .append(location.getLatitude())
                .append("\n")
                .append("Description: ")
                .append(location.getDescription())
                .append("\n");

        for (String propKey : location.getProperties().keySet()) {
            builder
                    .append(propKey)
                    .append(": ")
                    .append(location.getProperties().get(propKey));
        }

        System.out.println(builder.toString());
    }

    @Override
    public void displayNoLocationFound(String noLocationFoundMessage) {
        System.out.println(noLocationFoundMessage);
    }
}
