package ConcreteLocationLookupViews;

import ConcreteLocationLookupPresenters.viewInterfaces.AbstractLocationLookupViewFactory;
import ConcreteLocationLookupPresenters.viewInterfaces.LocationLookupView;

public class LocationLookupConsoleViewFactory extends AbstractLocationLookupViewFactory {
    @Override
    public LocationLookupView createView() {
        return new LocationLookupConsoleView();
    }
}
