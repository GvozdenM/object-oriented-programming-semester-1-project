package AbstractLocationSearchHistoryPresenter;

public abstract class LocationSearchHistoryPresenterFactory {
    public abstract AbstractLocationSearchHistoryPresenter create();
}
