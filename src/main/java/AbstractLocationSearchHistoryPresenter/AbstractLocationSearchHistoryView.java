package AbstractLocationSearchHistoryPresenter;

import java.util.Collection;

public interface AbstractLocationSerachHistoryView {
    void displayHistory(Collection<String> searchedLocations);
    void displayNoHistory();
}
