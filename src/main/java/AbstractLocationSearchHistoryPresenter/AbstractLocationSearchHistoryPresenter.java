package AbstractLocationSearchHistoryPresenter;

import java.util.Collection;

public abstract class AbstractLocationSearchHistoryPresenter {
    public abstract void presentHistory(Collection<String> searchedLocations);
}
