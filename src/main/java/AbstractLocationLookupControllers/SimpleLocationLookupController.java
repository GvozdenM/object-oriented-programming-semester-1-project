package AbstractLocationLookupControllers;


import Main.ControllerI;

import java.io.IOException;

public interface SimpleLocationLookupController extends ControllerI<String> {
    boolean process(String locationName) throws IOException;
}
