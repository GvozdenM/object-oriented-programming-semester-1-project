package AbstractWaitTimeControllers;

import Main.ControllerI;

import java.io.IOException;
import java.net.URISyntaxException;

public interface BusWaitTimeController extends ControllerI<String> {
    boolean process(String stopCode) throws IOException, URISyntaxException;
}
