package ConcreteRouteLookupPresenters;

import AbstractRouteLookupInteractors.RouteSectionResponse;
import AbstractRouteLookupPresenters.RouteLookupPresenter;
import AbstractRouteLookupInteractors.RouteLookupResponse;
import AbstractRouteLookupPresenters.RouteLookupViewModel;
import AbstractRouteModels.Route;
import ConcreteRouteLookupPresenters.viewInterfaces.RouteLookupScreenView;

import java.util.LinkedList;
import java.util.List;

public class RouteLookupScreenPresenter implements RouteLookupPresenter {
    private RouteLookupScreenView view;

    public RouteLookupScreenPresenter(RouteLookupScreenViewFactory viewFactory) {
        this.view = viewFactory.createView();
    }

    @Override
    public void presentRoute(RouteLookupResponse route) {
        RouteLookupViewModel vm = new RouteLookupViewModel(route.getTotalTimeMinutes());
        List<String> routeParts = new LinkedList<>();

        vm.setTotalTimeTaken(route.getTotalTimeMinutes());

        for(RouteSectionResponse section : route.getSectionList()) {
            StringBuilder builder = new StringBuilder();

            for(String metaPoint : section.getMetadata().keySet()) {
                builder
                        .append(metaPoint)
                        .append(" ")
                        .append(section.getMetadata().get(metaPoint));
            }

            addSectionTime(builder, section.getSectionTotalTimeMinutes());
            routeParts.add(builder.toString());
        }

        vm.setRouteParts(routeParts);

        view.displayHeader();
        view.displayTotalTime(vm);
        view.displayRouteParts(vm);
    }

    @Override
    public void presentInvalidParameterError() {
        this.view.displayInvalidParametersError();
    }

    @Override
    public void presentInvalidLocationError(RouteLookupResponse route) {
        this.view.displayLocationNotFoundError(
                new StringBuilder()
                    .append("Route from: ")
                    .append(route.getOriginName())
                    .append(" to ")
                    .append(route.getDestinationName())
                    .append(" could not be found! Invalid locations!")
                    .toString()
        );
    }

    private void addSectionTime(StringBuilder builder, int time) {
        builder.append(time);
        builder.append(" min");
    }
}
