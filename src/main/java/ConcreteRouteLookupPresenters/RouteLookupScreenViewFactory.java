package ConcreteRouteLookupPresenters;

import ConcreteRouteLookupPresenters.viewInterfaces.RouteLookupScreenView;

public abstract class RouteLookupScreenViewFactory {
    public abstract RouteLookupScreenView createView();
}
