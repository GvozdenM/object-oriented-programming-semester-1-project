package ConcreteRouteLookupPresenters.viewInterfaces;

import AbstractRouteLookupPresenters.RouteLookupViewModel;

public interface RouteLookupScreenView {
    void displayHeader();
    void displayTotalTime(RouteLookupViewModel route);
    void displayRouteParts(RouteLookupViewModel route);
    void displayInvalidParametersError();
    void displayLocationNotFoundError(String failedLocationPosition);
}
