package Main;

import AbstractLocationSearchHistoryController.AbstractLocationSearchHistoryController;
import ConcreteLocationLookupControllers.LocationLookupControllerFactory;
import ConcreteLocationSearchHistoryController.LocationSearchHistoryController;
import ConcreteLocationSearchHistoryPresenter.ConcreteLocationSearchHistoryPresenterFactory;
import ConcreteWaitTimeViews.viewInterfaces.BusWaitTimeViewFactory;
import AbstractWaitTimeMapping.WaitTimeDataGatewayFactory;
import AbstractWaitTimeInteractor.WaitTimeInteractorFactory;
import AbstractWaitTimePresenters.WaitTimePresenterFactory;
import AbstractWaitTimeMapping.TmbApiWaitTimeResponse;
import ConcreteWaitTimeRequestMaking.reponseLoading.GsonTmbApiWaitTimeResponseLoaderFactory;
import ConcreteWaitTimeInteractors.ConcreteWaitTimeInteractorFactory;
import ConcreteWaitTimeMapping.ConcreteWaitTimeDataGatewayFactory;
import ConcreteWaitTimePresenters.ScreenWaitTimePresenterFactory;
import ConcreteWaitTimeRequestMaking.WaitTimeRequestMakerFactory;
import ConcreteWaitTimeViews.WaitTimeConsoleViewFactory;
import LocationStoragePool.LocationStoragePoolProxy;
import LocationStoragePool.loading.transformation.DefaultLocationTransformChainFactory;
import LocationStoragePool.loading.transformation.LocationTransformationHandlerFactory;
import ConcreteUserStorage.UserStoragePool;
import AbstractLocationMapping.UnifiedLocationDataBank;
import ConcreteWaitTimeControllers.BusWaitTimeControllerFactory;
import AbstractNetworking.Connection.NetworkConnectionFactory;
import ConcreteNetworking.Connection.OkkHttpNetworkConnectionFactory;
import AbstractNetworking.RequestMaking.ApiRequestMakerFactory;
import AbstractUserStorage.UserFavoriteLocationDataGateway;
import utils.ClassLoading.GsonClassLoaderFactory;
import utils.TmbUrlConstructionDirectors.Abstract.TmpApiUrlDirectorFactory;
import utils.TmbUrlConstructionDirectors.concrete.WaitTimeUrlDirectorFactory;

import java.io.IOException;
import java.net.URISyntaxException;

public class Main {
    public static final
    LocationTransformationHandlerFactory locationTransformationHandlerFactory = new DefaultLocationTransformChainFactory();

    public static final UnifiedLocationDataBank locationDataGatewaySingleton = new LocationStoragePoolProxy(
        locationTransformationHandlerFactory
    );

    private static AbstractControllerFactory<String> busWaitTimeControllerFactory = new BusWaitTimeControllerFactory();
    public static final WaitTimePresenterFactory waitTimePresenterFactory = new ScreenWaitTimePresenterFactory();
    public static final WaitTimeInteractorFactory waitTimeInteractorFactory = new ConcreteWaitTimeInteractorFactory();
    private static final UserStoragePool userStoragePool = new UserStoragePool(
            "Adrian Bagati",
            "adriansucksass@gmail.com",
            1984);

    public static final BusWaitTimeViewFactory waitTimeViewFactory = new WaitTimeConsoleViewFactory();
    public static WaitTimeDataGatewayFactory waitTimeDataGatewayFactory = new ConcreteWaitTimeDataGatewayFactory();
    public static TmpApiUrlDirectorFactory<String> waitTimeUrlDirectorFactory = new WaitTimeUrlDirectorFactory();
    public static NetworkConnectionFactory networkConnectionFactory = new OkkHttpNetworkConnectionFactory();

    public static GsonClassLoaderFactory<TmbApiWaitTimeResponse> tmbApiWaitTimeResponseGsonClassLoaderFactory
            = new GsonTmbApiWaitTimeResponseLoaderFactory();

    public static ApiRequestMakerFactory<String, TmbApiWaitTimeResponse> waitTimeRequestMakerFactory
            = new WaitTimeRequestMakerFactory();

    public static final UserFavoriteLocationDataGateway favoriteLocationDataGateway = userStoragePool;
    private static LocationLookupControllerFactory locationLookupControllerFactory =
            new LocationLookupControllerFactory();

    public static void main(String[] args) {
        ControllerI<String> controller = busWaitTimeControllerFactory.createController();

        try {
            controller.process("1");
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }

        ControllerI<String> controllerI = locationLookupControllerFactory.createController(userStoragePool);

        try {
            controllerI.process("Plaça Catalunya");
            controllerI.process("Sagrada Família");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        AbstractLocationSearchHistoryController controller1 =
                new LocationSearchHistoryController(new ConcreteLocationSearchHistoryPresenterFactory());

        controller1.process(userStoragePool);

        cleanUp();
    }

    private static void cleanUp() {
        locationDataGatewaySingleton.close();
    }
}
