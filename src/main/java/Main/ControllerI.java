package Main;

import java.io.IOException;
import java.net.URISyntaxException;

public interface ControllerI<T> {
    boolean process(T input) throws IOException, URISyntaxException;
}
