package Main;

import AbstractUserStorage.UserLocationSearchRegistrationGateway;

public abstract class AbstractControllerFactory<T> {
    public abstract ControllerI<T> createController();
}
