package ConcreteRouteLookupViews;

import ConcreteRouteLookupPresenters.RouteLookupScreenViewFactory;
import ConcreteRouteLookupPresenters.viewInterfaces.RouteLookupScreenView;

public class RouteLookupConsoleViewFactory extends RouteLookupScreenViewFactory {
    @Override
    public RouteLookupScreenView createView() {
        return new RouteLookupConsoleView();
    }
}
