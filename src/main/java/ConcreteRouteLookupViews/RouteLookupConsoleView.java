package ConcreteRouteLookupViews;

import ConcreteRouteLookupPresenters.viewInterfaces.RouteLookupScreenView;
import AbstractRouteLookupPresenters.RouteLookupViewModel;

public class RouteLookupConsoleView implements RouteLookupScreenView {
    @Override
    public void displayHeader() {
        System.out.println("Fastest Combination: ");
    }

    @Override
    public void displayTotalTime(RouteLookupViewModel route) {
        System.out.println("Time Taken: ");
        System.out.println(route.getTotalTimeTaken());
    }

    @Override
    public void displayRouteParts(RouteLookupViewModel route) {
        System.out.println("Origin");
        this.displayRoutePartSeparator();

        for(String part : route.getRouteParts()) {
            System.out.println(part);
            this.displayRoutePartSeparator();
        }

        System.out.println("Destination");
    }

    @Override
    public void displayInvalidParametersError() {
        System.out.println("Invalid Input Parameters! :'(");
    }

    @Override
    public void displayLocationNotFoundError(String failedLocationPosition) {
        System.out.println(failedLocationPosition);
    }

    private void displayRoutePartSeparator() {
        System.out.println("|");
    }
}
