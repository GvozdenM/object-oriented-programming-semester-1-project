package AbstractRouteLookupPresenters;

import java.util.List;

public class RouteLookupViewModel {
    private int totalTimeTaken;
    private List<String> routeParts;

    public RouteLookupViewModel() {
    }

    public RouteLookupViewModel(int totalTimeTaken, List<String> routeParts) {
        this.totalTimeTaken = totalTimeTaken;
    }

    public RouteLookupViewModel(int totalTimeTaken) {
        this.totalTimeTaken = totalTimeTaken;
    }

    public int getTotalTimeTaken() {
        return totalTimeTaken;
    }

    public void setTotalTimeTaken(int totalTimeTaken) {
        this.totalTimeTaken = totalTimeTaken;
    }

    public List<String> getRouteParts() {
        return routeParts;
    }

    public void setRouteParts(List<String> routeParts) {
        this.routeParts = routeParts;
    }

    public void addRoutePart(String routePart) {
        this.routeParts.add(routePart);
    }
}
