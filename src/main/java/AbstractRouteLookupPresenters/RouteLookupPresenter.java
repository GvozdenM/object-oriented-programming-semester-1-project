package AbstractRouteLookupPresenters;

import AbstractRouteLookupInteractors.RouteLookupResponse;

public interface RouteLookupPresenter {
    void presentRoute(RouteLookupResponse route);
    void presentInvalidParameterError();
    void presentInvalidLocationError(RouteLookupResponse route);
}
