package AbstractLocationMapping;

import ConcreteLocationLookupInteractors.LocationDataGateway;

public abstract class UnifiedLocationDataGatewayFactory<T> {
    public abstract LocationDataGateway<T> createUnifiedLocationDataGateway();
}
