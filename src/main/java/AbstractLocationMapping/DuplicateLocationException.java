package AbstractLocationMapping;

public class DuplicateLocationException extends Exception {
    private String baseMessage = "DuplicateLocation: ";
    private String locationName;

    public DuplicateLocationException(String locationName) { this.locationName = locationName; }

    @Override
    public String toString() {
        return new StringBuilder()
                .append(baseMessage)
                .append("\n")
                .append(this.locationName)
                .append(": \"")
                .append("\"")
                .toString();
    }
}
