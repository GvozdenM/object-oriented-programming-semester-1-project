package AbstractLocationMapping;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Broad interface defining access methods for getting locations
 * from some storage space.
 */
public interface UnifiedLocationDataBank {
    /**
     * Returns all locations locations stored in this databank,
     * this includes the all of the current users locations
     * @return List of all registered locations
     */
    List<Map<String, String>> getAllLocations() throws IOException;

    /**
     * Returns all the user defined locations stored.
     * @return List of all hotels saved
     */
    List<Map<String, String>> getUserLocations() throws IOException;

    /**
     * Registers a new user defined location in the databank
     * @param location A new user defined location
     */
    void addLocation(Map<String, String> location) throws IOException;

    Map<String, String> getLocation(String name) throws IOException;

    void close();
}
