package ConcreteMenuPresenters;

import AbstractMenuPresenter.*;

import java.util.Arrays;


public class ConcreteMainMenuPresenter extends AbstractMenuPresenter {

    public ConcreteMainMenuPresenter(AbstractMenuViewFactory factory) {
        super(factory);
    }

    @Override
    public void presentMenu() {
        String[] menuElements = {
                "1. User Management",
                "2. Search Locations",
                "3. Plan a route",
                "4. Bus wait time",
                "5. Exit"
        };

        AbstractMenuView view = this.viewFactory.create();
        view.displayMenu(Arrays.asList(menuElements));
    }
}
