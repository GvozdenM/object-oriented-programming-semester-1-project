package ConcreteMenuPresenters;

import AbstractMenuPresenter.*;

import java.util.Arrays;

public class ConcreteUserMenuPresenter extends AbstractMenuPresenter {

    public ConcreteUserMenuPresenter(AbstractMenuViewFactory factory) {
        super(factory);
    }

    @Override
    public void presentMenu() {
        String[] menuElements = {
                "a) My locations",
                "b) Location History",
                "c) My routes",
                "d) Favourite stops and stations",
                "e) Stations inaugurated my birth year",
                "f) Back to principal menu",
        };
        AbstractMenuView view = this.viewFactory.create();
        view.displayMenu(Arrays.asList(menuElements));
    }
}
