package AbstractWaitTimeMapping;

public class TmbApiWaitTimeResponse {
    private IBusDataModel data;
    private String status;

    public TmbApiWaitTimeResponse(IBusDataModel data, String status) {
        this.data = data;
        this.status = status;
    }

    public IBusDataModel getData() {
        return data;
    }

    public void setData(IBusDataModel data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}