package AbstractWaitTimeMapping;

import com.google.gson.annotations.SerializedName;

public class IBusWaitTimeModel {
    private String line;
    private String routeId;
    @SerializedName("t-in-s")
    private int tins;
    @SerializedName("text-ca")
    private String textca;

    public IBusWaitTimeModel(String line, String routeId, int tins, String textca) {
        this.line = line;
        this.routeId = routeId;
        this.tins = tins;
        this.textca = textca;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public int getTins() {
        return tins;
    }

    public void setTins(int tins) {
        this.tins = tins;
    }

    public String getTextca() {
        return textca;
    }

    public void setTextca(String textca) {
        this.textca = textca;
    }
}
