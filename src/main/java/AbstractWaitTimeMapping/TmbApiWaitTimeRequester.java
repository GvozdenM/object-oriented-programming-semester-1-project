package AbstractWaitTimeMapping;

import java.io.IOException;
import java.net.URISyntaxException;

public interface TmbApiWaitTimeRequester {
    TmbApiWaitTimeResponse process(String request) throws URISyntaxException, IOException;
}
