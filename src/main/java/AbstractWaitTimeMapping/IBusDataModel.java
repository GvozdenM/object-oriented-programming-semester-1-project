package AbstractWaitTimeMapping;

import java.util.ArrayList;
import java.util.List;

public class IBusDataModel {
    private List<IBusWaitTimeModel> ibus;

    public IBusDataModel(List<IBusWaitTimeModel> ibus) {
        this.ibus = new ArrayList<>(ibus);
    }

    public List<IBusWaitTimeModel> getIbus() {
        return ibus;
    }

    public void setIbus(ArrayList<IBusWaitTimeModel> ibus) {
        this.ibus = ibus;
    }
}
