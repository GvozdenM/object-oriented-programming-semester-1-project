package AbstractWaitTimeMapping;

import ConcreteWaitTimeInteractors.WaitTimeDataGateway;

public abstract class WaitTimeDataGatewayFactory {
    public abstract WaitTimeDataGateway createGateway();
}
