package ConcreteWaitTimeMapping;

import AbstractWaitTimeMapping.WaitTimeDataGatewayFactory;
import ConcreteWaitTimeInteractors.WaitTimeDataGateway;
import Main.Main;

public class ConcreteWaitTimeDataGatewayFactory extends WaitTimeDataGatewayFactory {
    @Override
    public WaitTimeDataGateway createGateway() {
        return new TmbApiWaitTimeMapper(Main.waitTimeRequestMakerFactory);
    }
}
