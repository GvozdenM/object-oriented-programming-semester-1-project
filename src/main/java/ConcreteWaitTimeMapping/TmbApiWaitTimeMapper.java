package ConcreteWaitTimeMapping;

import AbstractWaitTimeMapping.IBusWaitTimeModel;
import AbstractWaitTimeMapping.TmbApiWaitTimeResponse;
import ConcreteWaitTimeInteractors.WaitTimeDataGateway;
import AbstractWaitTimeModels.WaitTime;
import AbstractNetworking.RequestMaking.ApiRequestMaker;
import AbstractNetworking.RequestMaking.ApiRequestMakerFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;

public class TmbApiWaitTimeMapper implements WaitTimeDataGateway {
    private ApiRequestMaker<String, TmbApiWaitTimeResponse> requestMaker;

    public TmbApiWaitTimeMapper(ApiRequestMakerFactory<String, TmbApiWaitTimeResponse> apiWaitTimeRequester) {
        this.requestMaker = apiWaitTimeRequester.createRequestMaker();
    }

    @Override
    public List<WaitTime> getWaitTime(String stopCode) throws IOException, URISyntaxException {
        TmbApiWaitTimeResponse apiResponse = requestMaker.process(stopCode);

        if(!"success".equals(apiResponse.getStatus())) {
            return new LinkedList<>();
        }
        else {
            return transformResponse(apiResponse);
        }
    }

    private List<WaitTime> transformResponse(TmbApiWaitTimeResponse responseModel) {
        List<WaitTime> waitTimes = new LinkedList<>();
        List<IBusWaitTimeModel> apiWaitTimes = responseModel.getData().getIbus();

        for(IBusWaitTimeModel apiWaitTime : apiWaitTimes) {
            waitTimes.add(new WaitTime(
                    apiWaitTime.getLine(),
                    apiWaitTime.getRouteId(),
                    (int) Math.ceil(apiWaitTime.getTins() / 60.00),
                    apiWaitTime.getTins()
            ));
        }

        return waitTimes;
    }
}
