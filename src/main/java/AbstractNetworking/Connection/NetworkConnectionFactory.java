package AbstractNetworking.Connection;

public abstract class NetworkConnectionFactory {
    public abstract NetworkConnection createNetworkConnection();
}
