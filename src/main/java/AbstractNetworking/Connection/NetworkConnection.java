package AbstractNetworking.Connection;

import java.io.IOException;
import java.net.URL;

public interface NetworkConnection {
    String get(URL request) throws IOException;
}
