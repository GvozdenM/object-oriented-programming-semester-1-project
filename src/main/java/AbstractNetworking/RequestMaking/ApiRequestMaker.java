package AbstractNetworking.RequestMaking;

import AbstractNetworking.Connection.NetworkConnection;
import AbstractNetworking.Connection.NetworkConnectionFactory;
import org.apache.http.client.utils.URIBuilder;
import utils.ClassLoading.GsonClassLoader;
import utils.ClassLoading.GsonClassLoaderFactory;
import utils.TmbUrlConstructionDirectors.Abstract.TmbApiUrlDirector;
import utils.TmbUrlConstructionDirectors.Abstract.TmpApiUrlDirectorFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

public abstract class ApiRequestMaker<Req, Res> {
    private TmpApiUrlDirectorFactory<Req> urlDirectorFactory;
    private NetworkConnectionFactory networkConnectionFactory;
    private GsonClassLoaderFactory<Res> gsonClassLoaderFactory;


    public ApiRequestMaker(TmpApiUrlDirectorFactory<Req> urlDirectorFactory,
                           NetworkConnectionFactory networkConnectionFactory,
                           GsonClassLoaderFactory<Res> gsonClassLoaderFactory) {

        this.urlDirectorFactory = urlDirectorFactory;
        this.networkConnectionFactory = networkConnectionFactory;
        this.gsonClassLoaderFactory = gsonClassLoaderFactory;
    }

    public Res process(Req request) throws URISyntaxException, IOException {
        //configuring url director and builder
        URIBuilder builder = new URIBuilder();
        TmbApiUrlDirector<Req> director = this.urlDirectorFactory.createUrlDirector(builder);

        //instantiating network connection and response loading objects
        NetworkConnection networkConnection = this.networkConnectionFactory.createNetworkConnection();
        GsonClassLoader<Res> loader = gsonClassLoaderFactory.createLoader();

        //constructing URL
        director.constructUrl(request);
        URL url = builder
                .build()
                .toURL();

        String response = networkConnection.get(url);

        return loader.load(response);
    }
}
