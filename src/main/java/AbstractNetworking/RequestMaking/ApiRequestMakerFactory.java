package AbstractNetworking.RequestMaking;

public abstract class ApiRequestMakerFactory<Req, Res> {
    public abstract ApiRequestMaker<Req, Res> createRequestMaker();
}
